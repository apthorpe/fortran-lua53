co = coroutine.create(function(a)
       d = a
       for i = 1, 10 do
         print("co", i, " <- ", d)
         d = coroutine.yield(i)
       end
     end)

-- Drive the coroutine
b = 0
c = b + 1
print("Initial state: ", coroutine.status(co))
-- can_go, b = coroutine.resume(co, c)
while coroutine.status(co) == "suspended" do
  print("b = ", b)
  print("c = ", c)
  print("State: ", coroutine.status(co))
  can_go, b = coroutine.resume(co, c)
  if b ~= nil then
    c = b + 1
  else
    print("Bailing; b is nil")
    break
  end
end
print("Final state: ", coroutine.status(co))
print("b = ", b)
print("c = ", c)
