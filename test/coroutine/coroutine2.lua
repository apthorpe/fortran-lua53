-- co is a global coroutine; let Fortran manipulate it with resume
co = coroutine.create(function(a)
       d = a
       for i = 1, 10 do
         print("co", i, " <- ", d)
         d = coroutine.yield(i)
       end
     end)