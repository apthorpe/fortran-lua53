!> @file ut_error1.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Lua Error test 1

!> @brief Tests `lua_error` function
program ut_error1
    use, intrinsic :: iso_fortran_env, only: WP => REAL64,              &
        stdout=>OUTPUT_UNIT
    use, intrinsic :: iso_c_binding, only: c_ptr, c_null_ptr,           &
        c_null_char
    use :: toast
    use :: lua
    implicit none

    ! Lua state object
    type(c_ptr)         :: l

    integer :: retval
    integer :: id

    type(TestCase) :: test

    continue

    call test%init(name="ut_error1")

    !!! Test exposed functions

    l = lual_newstate()
    call lual_openlibs(l)

    ! This should never return
    retval = lua_error(l)

    ! The remainder of this program should never be executed
    id = lua_version(c_null_ptr)
    write(unit=stdout, fmt='(A, I8)') "Lua version ", id
    call test%asserttrue(id > 0, message="lua_version() > 0")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_error1.json")

    call test%checkfailure()
end program ut_error1
