!> @file ut_string.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit test replicating the string example

!> @brief Unit test replicating the string example
program ut_string

    use, intrinsic :: iso_c_binding, only: c_ptr, c_null_char
    use :: lua
    use :: toast
    implicit none

    ! Lua state object
    type(c_ptr) :: l

    ! Result code
    integer     :: rc

    ! TOAST test object
    type(TestCase) :: test

    continue

    call test%init(name="ut_string")

    !!! Test exposed functions

    ! Instrumented replication of string example program

    l = lual_newstate()
    call lual_openlibs(l)

    rc = lual_loadstring(l, 'print("Hello from Lua!")')
    call test%assertequal(rc, 0, message="Success (0) loading Lua code fragment")

    rc = lua_pcall(l, 0, 0, 0)
    call test%assertequal(rc, 0, message="Success running preloaded code fragment")


    ! Similarly, using lual_dostring (combines loadstring and pcall)
    rc = lual_dostring(l, 'print("Hello again!")' // c_null_char)
    call test%assertequal(rc, 0, message="Success (0) directly running Lua code fragment")

    call lua_close(l)

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_string.json")

    call test%checkfailure()
end program ut_string
