!> @file ut_arith.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Test of arithmetic Lua C API operations

!! @brief Test of arithmetic Lua C API operations
program ut_arith
    use, intrinsic :: iso_fortran_env, only: WP => REAL64, INT64,       &
        stdout => OUTPUT_UNIT
    use, intrinsic :: iso_c_binding
    use :: lua
    use :: toast
    implicit none

    ! Lua state object
    type(c_ptr) :: l

    integer :: i
    integer :: nstack
    integer :: idx

    type(TestCase) :: test

    continue

    call test%init(name="ut_arith")

    !!! Test exposed functions

    l = lual_newstate()
    call test%asserttrue(c_associated(l), message="Expect pointer to Lua state")

    call lual_openlibs(l)

    ! Check that version is at least 5.3 (503)
    call test%asserttrue(lua_version(l) >= 503_INT64, message="Expect version is at least 5.3")

    nstack = lua_gettop(l)
    call test%assertequal(nstack, 0, message="Expect empty stack at start")

    ! Operations to test
    ! !> Lua arithmetic addition operator `+`
    ! integer(kind=c_int), parameter, public :: LUA_OPADD  = 0

    call lua_pushinteger(l, 10_c_long_long)
    call lua_pushinteger(l, -7_c_long_long)
    nstack = lua_gettop(l)
    call test%assertequal(nstack, 2, message="Expect two items on stack")

    call lua_arith(l, LUA_OPADD)
    nstack = lua_gettop(l)
    call test%assertequal(nstack, 1, message="Expect one item on stack")

    call test%assertequal(3_c_long_long, lua_tointeger(l, -1), message="10 + (-7) = 3")

    call lua_pop(l, -1)

    call test_iop(l, 10, -7,   3, LUA_OPADD,  "+")
    call test_iop(l, 10, -7,  17, LUA_OPSUB,  "-")
    call test_iop(l, 10, -7, -70, LUA_OPMUL,  "*")

    ! Note: This is 0 since 10/(-7) cannot be represented as an integer; lua_tointeger(-1.4...) = 0
    call test_iop(l, 10, -7,   0, LUA_OPDIV,  "/")
    ! Note: This is 10/(-7) interpreted as a Lua number (c_double)
    call test_i2rop(l, 10, -7, (-10.0_WP / 7.0_WP), LUA_OPDIV,  "/")

    call test_iop(l, 10, -7,  -2, LUA_OPIDIV, "//")
    call test_iop(l, 10, -7,  -4, LUA_OPMOD,  "%")

    call test_iop(l,  2,  3,   8, LUA_OPPOW,  "^")
    call test_iop(l,  3,  2,   9, LUA_OPPOW,  "^")

    call test_iop(l, -7, 10,   3, LUA_OPADD, "+")
    call test_iop(l, -7, 10, -17, LUA_OPSUB, "-")
    call test_iop(l, -7, 10, -70, LUA_OPMUL, "*")
    call test_iop(l, -7, 10,   0, LUA_OPDIV,  "/")
    call test_iop(l, -7, 10,  -1, LUA_OPIDIV, "//")
    call test_iop(l, -7, 10,   3, LUA_OPMOD,  "%")

    call test_rop(l, 10.0_WP, -7.0_WP,   3.0_WP, LUA_OPADD, "+")
    call test_rop(l, 10.0_WP, -7.0_WP,  17.0_WP, LUA_OPSUB, "-")
    call test_rop(l, 10.0_WP, -7.0_WP, -70.0_WP, LUA_OPMUL, "*")
    call test_rop(l, 10.0_WP, -7.0_WP, -(10.0_WP / 7.0_WP), LUA_OPDIV, "/")
    call test_rop(l, 10.0_WP, -7.0_WP, real(floor(10.0_WP / (-7.0_WP)), kind=WP), LUA_OPIDIV, "//")

    call test_rop(l, -7.0_WP, 10.0_WP,   3.0_WP, LUA_OPADD, "+")
    call test_rop(l, -7.0_WP, 10.0_WP, -17.0_WP, LUA_OPSUB, "-")
    call test_rop(l, -7.0_WP, 10.0_WP, -70.0_WP, LUA_OPMUL, "*")
    call test_rop(l, -7.0_WP, 10.0_WP,  -0.7_WP, LUA_OPDIV, "/")
    call test_rop(l, -7.0_WP, 10.0_WP,  -1.0_WP, LUA_OPIDIV, "//")

    call test_rop(l, 49.0_WP,  0.5_WP,   7.0_WP,   LUA_OPPOW, "^")
    call test_rop(l,  2.0_WP, -3.0_WP,   0.125_WP, LUA_OPPOW, "^")

    call test_rop(l,  0.0_WP, (2.0_WP / 7.0_WP), -(2.0_WP / 7.0_WP), LUA_OPUNM, "XXX unary -")
    call test_rop(l,  0.0_WP,  0.0_WP,   0.0_WP,   LUA_OPUNM, "XXX unary -")
    call test_rop(l,  0.0_WP, -0.713_WP, 0.713_WP, LUA_OPUNM, "XXX unary -")

    ! Test lua_compare

    call lua_pushnumber( l,   -7.0_c_double)     ! 1   -8
    call lua_pushnumber( l,   -7.1_c_double)     ! 2   -7
    call lua_pushnumber( l,   10.0_c_double)     ! 3   -6
    call lua_pushnumber( l,   -7.0_c_double)     ! 4   -5
    call lua_pushnumber( l,    0.0_c_double)     ! 5   -4
    call lua_pushinteger(l,  65536_c_long_long)  ! 6   -3
    call lua_pushinteger(l,      0_c_long_long)  ! 7   -2
    call lua_pushinteger(l, -65536_c_long_long)  ! 8   -1

    call test%assertfalse(lua_compare(l, 1, 2, LUA_OPLT), message="-7.0 /< -7.1")
    call test%assertfalse(lua_compare(l, 1, 2, LUA_OPLE), message="-7.0 /<= -7.1")
    call test%assertfalse(lua_compare(l, 1, 2, LUA_OPEQ), message="-7.0 /= -7.1")

    call test%asserttrue(lua_compare(l, 2, 1, LUA_OPLT), message="-7.1 < -7.0")
    call test%asserttrue(lua_compare(l, 2, 1, LUA_OPLE), message="-7.1 <= -7.0")
    call test%assertfalse(lua_compare(l, 2, 1, LUA_OPEQ), message="-7.1 /= -7.0")

    call test%asserttrue(lua_compare(l, -4, -2, LUA_OPEQ), message="0.0 == 0")
    call test%asserttrue(lua_compare(l,  7, -4, LUA_OPEQ), message="0.0 == 0")
    call test%asserttrue(lua_compare(l,  1, -5, LUA_OPEQ), message="-7.0 == -7.0")
    call test%asserttrue(lua_compare(l,  4, -8, LUA_OPEQ), message="-7.0 == -7.0")

    ! !> Lua arithmetic subtraction operator `-`
    ! integer(kind=c_int), parameter, public :: LUA_OPSUB  = 1
    ! !> Lua arithmetic multiplication operator `*`
    ! integer(kind=c_int), parameter, public :: LUA_OPMUL  = 2
    ! !> Lua arithmetic modulo operator `%`
    ! integer(kind=c_int), parameter, public :: LUA_OPMOD  = 3
    ! !> Lua arithmetic exponentiation operator `^`
    ! integer(kind=c_int), parameter, public :: LUA_OPPOW  = 4
    ! !> Lua arithmetic division operator `/`
    ! integer(kind=c_int), parameter, public :: LUA_OPDIV  = 5
    ! !> Lua arithmetic 'floor division' operator `//`
    ! integer(kind=c_int), parameter, public :: LUA_OPIDIV = 6
    ! !> Lua bitwise-AND operator `&`
    ! integer(kind=c_int), parameter, public :: LUA_OPBAND = 7
    ! !> Lua bitwise-OR operator `|`
    ! integer(kind=c_int), parameter, public :: LUA_OPBOR  = 8
    ! !> Lua bitwise-XOR operator '~'
    ! integer(kind=c_int), parameter, public :: LUA_OPBXOR = 9
    ! !> Lua bitwise left shift operator `<<`
    ! integer(kind=c_int), parameter, public :: LUA_OPSHL  = 10
    ! !> Lua bitwise right shift operator `>>`
    ! integer(kind=c_int), parameter, public :: LUA_OPSHR  = 11
    ! !> Lua arithmetic unary minus operator `-`
    ! integer(kind=c_int), parameter, public :: LUA_OPUNM  = 12
    ! !> Lua bitwise-NOT operator `~`
    ! integer(kind=c_int), parameter, public :: LUA_OPBNOT = 13

    ! ! lua_dofile() success
    ! fname = 'utility.lua'
    ! rc = lual_dofile(l, trim(fname))

    ! call test%assertequal(rc, 0, message="Success (0) loading " // trim(fname))

    ! nstack = lua_gettop(l)
    ! call test%assertequal(nstack, 0, message="Expect empty stack after lua_dofile()")

    ! nstack = lua_gettop(l)
    ! call test%assertequal(nstack, 1, message="Expect one element on stack")

    ! ! Stop this embarrassment...
    ! call lua_pop(l, 1)

    ! Clear the stack
    call lua_settop(l, 0_c_int)

    ! Poke at the garbage collector

    i = 0
    idx = int(lua_gc(l, LUA_GCCOUNT, int(i, kind=c_int)))
    ! write(unit=stdout, fmt='("Lua is using ", I0, " kB")') idx
    call test%asserttrue(idx > 0, message="Lua is using some memory")

    idx = int(lua_gc(l, LUA_GCCOLLECT, int(i, kind=c_int)))
    ! write(unit=stdout, fmt='("GC returned ", I0)') idx
    call test%assertequal(idx, 0, message="Expect GC(collect) to return 0")

    idx = int(lua_gc(l, LUA_GCCOUNT, int(i, kind=c_int)))
    ! write(unit=stdout, fmt='("Lua is using ", I0, " kB")') idx
    call test%asserttrue(idx > 0, message="Lua is still using some memory")

    ! Close the interpreter

    nstack = lua_gettop(l)
    call test%assertequal(nstack, 0, message="Expect empty stack after all tests")

    call lua_close(l)

    ! Q: Should l be c_null_ptr once it's closed?
!    call test%assertfalse(c_associated(l), message="Expect null pointer after closeing Lua state")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_arith.json")

    call test%checkfailure()

contains

subroutine test_iop(l, a, b, aob, op, opsym)
    !> Lua state
    type(c_ptr), intent(in) :: l
    !> Operand 1
    integer, intent(in) :: a
    !> Operand 2
    integer, intent(in) :: b
    !> Expected result of op(a, b)
    integer, intent(in) :: aob
    !> Lua arithmetic/bitwise operation code
    integer(kind=c_int), intent(in) :: op
    !> Symbol(s) representing operation
    character(len=*), intent(in) :: opsym

    integer(kind=c_long_long) :: ia
    integer(kind=c_long_long) :: ib
    integer(kind=c_long_long) :: iaob
    integer(kind=c_long_long) :: iresult
    character(len=80) :: testtag

1   format('(', I0, ') ', A, ' (', I0, ') = ', I0)

    continue

    ia   = int(a,   kind=c_long_long)
    ib   = int(b,   kind=c_long_long)
    iaob = int(aob, kind=c_long_long)

    if (op == LUA_OPUNM) then
        call lua_pushinteger(l, ib)
        call test%assertequal(lua_gettop(l), 1, message="Expect one item on stack")
    else
        call lua_pushinteger(l, ia)
        call lua_pushinteger(l, ib)
        call test%assertequal(lua_gettop(l), 2, message="Expect two items on stack")
    end if

    call lua_arith(l, op)

    nstack = lua_gettop(l)
    call test%assertequal(lua_gettop(l), 1, message="Expect one item on stack")

    iresult = lua_tointeger(l, -1)
    write(unit=testtag, fmt=1) ia, opsym, ib, iaob
    write(unit=stdout, fmt=1) ia, opsym, ib, iaob
    write(unit=stdout, fmt=1) ia, opsym, ib, iresult
    call test%assertequal(iresult, iaob, message=trim(testtag))

    call lua_pop(l, -1)

    return
end subroutine test_iop

subroutine test_i2rop(l, a, b, aob, op, opsym)
    !> Lua state
    type(c_ptr), intent(in) :: l
    !> Operand 1
    integer, intent(in) :: a
    !> Operand 2
    integer, intent(in) :: b
    !> Expected result of op(a, b)
    real(kind=WP), intent(in) :: aob
    !> Lua arithmetic/bitwise operation code
    integer(kind=c_int), intent(in) :: op
    !> Symbol(s) representing operation
    character(len=*), intent(in) :: opsym

    integer(kind=c_long_long) :: ia
    integer(kind=c_long_long) :: ib
    real(kind=c_double) :: raob
    real(kind=c_double) :: rresult
    character(len=80) :: testtag

1   format('(', I0, ') ', A, ' (', I0, ') = ', ES12.4)

    continue

    ia   = int(a,    kind=c_long_long)
    ib   = int(b,    kind=c_long_long)
    raob = real(aob, kind=c_double)

    if (op == LUA_OPUNM) then
        call lua_pushinteger(l, ib)
        call test%assertequal(lua_gettop(l), 1, message="Expect one item on stack")
    else
        call lua_pushinteger(l, ia)
        call lua_pushinteger(l, ib)
        call test%assertequal(lua_gettop(l), 2, message="Expect two items on stack")
    end if

    call lua_arith(l, op)

    nstack = lua_gettop(l)
    call test%assertequal(lua_gettop(l), 1, message="Expect one item on stack")

    rresult = lua_tonumber(l, -1)
    write(unit=testtag, fmt=1) ia, opsym, ib, raob
    ! write(unit=stdout, fmt=1) ia, opsym, ib, raob
    ! write(unit=stdout, fmt=1) ia, opsym, ib, rresult
    call test%assertequal(rresult, raob, message=trim(testtag))

    call lua_pop(l, -1)

    return
end subroutine test_i2rop

subroutine test_rop(l, a, b, aob, op, opsym)
    !> Lua state
    type(c_ptr), intent(in) :: l
    !> Operand 1
    real(kind=WP), intent(in) :: a
    !> Operand 2
    real(kind=WP), intent(in) :: b
    !> Expected result of op(a, b)
    real(kind=WP), intent(in) :: aob
    !> Lua arithmetic/bitwise operation code
    integer(kind=c_int), intent(in) :: op
    !> Symbol(s) representing operation
    character(len=*), intent(in) :: opsym

    real(kind=c_double) :: ra
    real(kind=c_double) :: rb
    real(kind=c_double) :: raob
    real(kind=c_double) :: rresult
    character(len=80) :: testtag

1   format('(', ES12.4, ') ', A, ' (', ES12.4, ') = ', ES12.4)

    continue

    ra   = real(a,   kind=c_double)
    rb   = real(b,   kind=c_double)
    raob = real(aob, kind=c_double)

    if (op == LUA_OPUNM) then
        call lua_pushnumber(l, rb)
        call test%assertequal(lua_gettop(l), 1, message="Expect one item on stack")
    else
        call lua_pushnumber(l, ra)
        call lua_pushnumber(l, rb)
        call test%assertequal(lua_gettop(l), 2, message="Expect two items on stack")
    end if

    call lua_arith(l, op)

    call test%assertequal(lua_gettop(l), 1, message="Expect one item on stack")

    rresult = lua_tonumber(l, -1)
    write(unit=testtag, fmt=1) ra, opsym, rb, raob
    call test%assertequal(rresult, raob, message=trim(testtag))

    call lua_pop(l, -1)

    return
end subroutine test_rop

end program ut_arith
