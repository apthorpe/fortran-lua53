!> @file ut_error2.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Lua Error test 2

!> @brief !> @brief Tests `luaL_error` function
program ut_error2
    use, intrinsic :: iso_fortran_env, only: WP => REAL64,              &
        stdout=>OUTPUT_UNIT
    use, intrinsic :: iso_c_binding, only: c_ptr, c_char, c_null_ptr,   &
        c_null_char
    use :: toast
    use :: lua
    implicit none

    ! Lua state object
    type(c_ptr)         :: l

    character(kind=c_char, len=:), allocatable :: errmsg

    integer :: retval
    integer :: id

    type(TestCase) :: test

    continue

    call test%init(name="ut_error2")

    !!! Test exposed functions

    l = lual_newstate()
    call lual_openlibs(l)

    errmsg = "Error triggered from Fortran" // c_null_char

    ! This should never return
    retval = lual_error(l, errmsg)

    ! The remainder of this program should never be executed
    id = lua_version(c_null_ptr)
    write(unit=stdout, fmt='(A, I8)') "Lua version ", id
    call test%asserttrue(id > 0, message="lua_version() > 0")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_error2.json")

    call test%checkfailure()
end program ut_error2
