!> @file ut_coroutine.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit test demonstrating coroutines

!> @brief Unit test demonstrating coroutines
program ut_coroutine
    use, intrinsic :: iso_fortran_env, only: stdout => output_unit, INT64
    use, intrinsic :: iso_c_binding!, only: c_ptr, c_null_char
    use :: lua
    use :: toast
    implicit none

    ! Lua state object
    type(c_ptr) :: l
    type(c_ptr) :: lco

    ! Lua source file name
    character(len=:), allocatable :: fname

    ! Result code
    integer     :: rc

    integer     :: i
    integer     :: nstack
    integer     :: ncostack

    integer(kind=INT64) :: b
    integer(kind=INT64) :: c

    character(len=:), allocatable :: tmpstr
    integer     :: strsize

    logical     :: tf

    ! TOAST test object
    type(TestCase) :: test

    continue

    call test%init(name="ut_coroutine")

    !!! Test exposed functions

    ! Instrumented replication of coroutine example program

    write(unit=stdout, fmt="('*** Starting test, running coroutine1.lua ***')")

    l = lual_newstate()
    call lual_openlibs(l)

    ! luasrc = (/ &
    ! 'co = coroutine.create(function()                                                ', &
    ! '       for i = 1, 10 do                                                         ', &
    ! '         print("co", i)                                                         ', &
    ! '         coroutine.yield()                                                      ', &
    ! '       end                                                                      ', &
    ! '     end)                                                                       '  &
    ! /)

    ! nsize = size(luasrc)
    ! do i = 1, nsize
    !     write(unit=stdout, fmt='("Loading fragment ", I0, " of ", I0)') i, nsize
    !     rc = lual_loadstring(l, trim(luasrc(i)))
    !     call test%assertequal(rc, 0, message="Success (0) loading multiline Lua code fragment")
    ! end do

    ! rc = lua_pcall(l, 0, 0, 0)
    ! call test%assertequal(rc, 0, message="Success running preloaded multiline code fragment")

    ! Run Lua driver code which ping-pongs off global coroutine 'co'
    fname = 'coroutine1.lua'
    rc = lual_dofile(l, fname)

    call test%asserttrue(rc == 0, message="Expected success running " // fname)
    ! call test%asserttrue(rc /= 0, message="Expected failire running " // fname)

    call lua_close(l)

    ! Start with fresh thread
    write(unit=stdout, fmt="('*** Restarting with new thread, loading coroutine2.lua ***')")

    l = lual_newstate()
    call lual_openlibs(l)

    ! ! Check yieldability directly
    ! call test%asserttrue(lua_isyieldable(l), message="lua_isyieldable(l) is .true.")

    fname = 'coroutine2.lua'
    rc = lual_dofile(l, fname)
    call test%asserttrue(rc == 0, message="Expected success running " // fname)

    ! Have Fortran ping-pong off global Lua coroutine 'co'

    ! Put global 'co' on stack, return type
    rc = lua_getglobal(l, 'co')
    call test%assertequal(rc, LUA_TTHREAD, message="co is type LUA_THREAD")

    ! Check type directly
    tf = lua_isthread(l, -1)
    call test%asserttrue(tf, message="lua_isthread(co) is .true.")

    if (tf) then
        write(unit=stdout, fmt="('Creating lco as thread')")
        lco = lua_tothread(l, -1)
        call lua_pop(l, -1)

        ! Check yieldability directly
        call test%assertfalse(lua_isyieldable(lco), message="lua_isyieldable(co) is .false.")

        rc = lua_status(lco)
        select case(rc)
        case(LUA_TH_OK)
            write(unit=stdout, fmt="('lco status is LUA_OK: ', I0)") rc
        case(LUA_TH_YIELD)
            write(unit=stdout, fmt="('lco status is LUA_YIELD (suspended): ', I0)") rc
        case default
            write(unit=stdout, fmt="('lco status is ', I0)") rc
        end select

        rc = lua_status(l)
        select case(rc)
        case(LUA_TH_OK)
            write(unit=stdout, fmt="('l status is LUA_OK: ', I0)") rc
        case(LUA_TH_YIELD)
            write(unit=stdout, fmt="('l status is LUA_YIELD (suspended): ', I0)") rc
        case default
            write(unit=stdout, fmt="('l status is ', I0)") rc
        end select

        b = 0_INT64
        c = b + 1_INT64
PINGPONG: do while (lua_status(lco) == LUA_TH_OK .or. lua_status(lco) == LUA_TH_YIELD)

            write(unit=stdout, fmt="('b = ', I0)") b
            write(unit=stdout, fmt="('c = ', I0)") c
            rc = lua_status(lco)

            select case(rc)
            case(LUA_TH_OK)
                write(unit=stdout, fmt="('State: LUA_OK: ', I0)") rc
            case(LUA_TH_YIELD)
                write(unit=stdout, fmt="('State: LUA_YIELD (suspended): ', I0)") rc
            case default
                write(unit=stdout, fmt="('State: (dead?): ', I0)") rc
            end select

            call lua_pushinteger(lco, c)
            ! Resume with one argument (c; on top of stack)
            rc = lua_resume(lco, l, 1)
            write(unit=stdout, fmt="('lua_resume(lco, l, 1) -> rc = ', I0)") rc
            !NO! double-free! call lua_close(lco)

            rc = lua_status(l)
            select case(rc)
            case(LUA_TH_OK)
                write(unit=stdout, fmt="('l status is LUA_OK: ', I0)") rc
            case(LUA_TH_YIELD)
                write(unit=stdout, fmt="('l status is LUA_YIELD (suspended): ', I0)") rc
            case default
                write(unit=stdout, fmt="('l status is ', I0)") rc
            end select

            rc = lua_status(lco)
            select case(rc)
            case(LUA_TH_OK)
                write(unit=stdout, fmt="('lco status is LUA_OK: ', I0)") rc
            case(LUA_TH_YIELD)
                write(unit=stdout, fmt="('lco status is LUA_YIELD (suspended): ', I0)") rc
            case default
                write(unit=stdout, fmt="('lco status is ', I0)") rc
            end select

            ncostack = lua_gettop(lco)
            write(unit=stdout, fmt="('n(lco) = ', I0)") ncostack
            do i = 1, ncostack
                tmpstr = lual_tolstring(lco, -i, strsize)
                write(unit=stdout, fmt='("Stack loc -", I0, " is a ", A, ": ", A)') &
                    i, lual_typename(lco, -i), tmpstr
            end do

            nstack = lua_gettop(l)
            write(unit=stdout, fmt="('n(l) = ', I0)") nstack
            do i = 1, nstack
                tmpstr = lual_tolstring(l, -i, strsize)
                write(unit=stdout, fmt='("Stack loc -", I0, " is a ", A, ": ", A)') &
                    i, lual_typename(l, -i), tmpstr
            end do

            ! if (lua_isthread(l, -1)) then
            !     write(unit=stdout, fmt='("Moving thread from l(-1) to lco")')
            !     lco = lua_tothread(l, -1)
            !     call lua_pop(l, -1)
            ! end if

!            if (lua_isinteger(lco, -1)) then
            if (lua_isnoneornil(lco, -1)) then
                write(unit=stdout, fmt='(A)') "none/nil returned by "   &
                    // "resume, coroutine has finished. Quitting."
                exit PINGPONG
            else
                b = lua_tointeger(lco, -1)
                c = b + 1_INT64
            end if
        end do PINGPONG

        write(unit=stdout, fmt="('--- Status at completion of coroutine ---')")

        write(unit=stdout, fmt="('b is nil')")
        write(unit=stdout, fmt="('c = ', I0)") c

        rc = lua_status(l)
        select case(rc)
        case(LUA_TH_OK)
            write(unit=stdout, fmt="('l status is LUA_OK: ', I0)") rc
        case(LUA_TH_YIELD)
            write(unit=stdout, fmt="('l status is LUA_YIELD (suspended): ', I0)") rc
        case default
            write(unit=stdout, fmt="('l status is ', I0)") rc
        end select

        nstack = lua_gettop(l)
        write(unit=stdout, fmt="('n(l) = ', I0)") nstack
        do i = 1, nstack
            tmpstr = lual_tolstring(l, -i, strsize)
            write(unit=stdout, fmt='("Stack loc -", I0, " is a ", A, ": ", A)') &
            i, lual_typename(l, -i), tmpstr
        end do

        rc = lua_status(lco)
        select case(rc)
        case(LUA_TH_OK)
            write(unit=stdout, fmt="('lco status is LUA_OK: ', I0)") rc
        case(LUA_TH_YIELD)
            write(unit=stdout, fmt="('lco status is LUA_YIELD (suspended): ', I0)") rc
        case default
            write(unit=stdout, fmt="('lco status is ', I0)") rc
        end select

        ncostack = lua_gettop(lco)
        write(unit=stdout, fmt="('n(lco) = ', I0)") ncostack
        do i = 1, ncostack
            tmpstr = lual_tolstring(lco, -i, strsize)
            write(unit=stdout, fmt='("Stack loc -", I0, " is a ", A, ": ", A)') &
            i, lual_typename(lco, -i), tmpstr
        end do

        ! Check yieldability directly
        call test%assertfalse(lua_isyieldable(lco), message="lua_isyieldable(co) is .false.")

    ! tf = lua_isnumber(l, -1)
        ! call test%asserttrue(tf, message="Top of stack is a number")

        ! tf = lua_isboolean(l, -1)
        ! call test%asserttrue(tf, message="Top of stack is a boolean")

        ! tf = lua_isthread(l, -1)
        ! call test%asserttrue(tf, message="Top of stack is a thread")

        ! tf = lua_isnumber(l, -2)
        ! call test%asserttrue(tf, message="Stack(-2) is a number")

        ! tf = lua_isboolean(l, -2)
        ! call test%asserttrue(tf, message="Stack(-2) is a boolean")

        ! tf = lua_isthread(l, -2)
        ! call test%asserttrue(tf, message="Stack(-2) is a thread")
    end if

    ! Check thread status

    call lua_close(l)

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "ut_coroutine.json")

    call test%checkfailure()
end program ut_coroutine
