!> @file lua.f90
!!
!! @brief A collection of ISO C binding interfaces to Lua 5.3 for
!! Fortran 2003.
!!
!! @author Philipp Engel
!! @copyright ISC; see LICENCE

!> @brief A collection of ISO C binding interfaces to Lua 5.3 for
!! Fortran 2003.
!!
!! C API functions which are not inmplemented:
!!
!!  * `lua_atpanic` - unclear how to support panic functions in Fortran
!!  * `lua_dump` - may be desirable but requires additional C coding
!!  * `lua_getallocf` - manipulation of `lua_Alloc` not supported
!!  * `lua_getextraspace` - requires `LUA_EXTRASPACE` from `lua.h`; not exported
!!  * `lua_newstate` - use `luaL_newstate`, manipulation of `lua_Alloc` not supported
!!  * `lua_pushfstring` - fstring compositing should be done in Fortran (can't support variadic functions)
!!  * `lua_pushglobaltable` - requires `LUA_REGISTRYINDEX` and `LUA_RIDX_GLOBALS` (= 2) from `lua.h`; not exported.
!!  * `lua_pushvfstring` - fstring compositing should be done in Fortran (can't support variadic functions)
!!  * `lua_setallocf` - manipulation of `lua_Alloc` not supported
!!  * `lua_upvalueindex` - requires `LUA_REGISTRYINDEX` from `lua.h`; not exported. See `luaL_getmetatable`
!!  * `lua_pushunsigned` - deprecated in Lua 5.3
!!  * `lua_tounsigned` - deprecated in Lua 5.3
!!  * `lua_tounsignedx` - deprecated in Lua 5.3
!!
!! Debug API functions which are not inmplemented:
!!
!!  * `lua_gethook`
!!  * `lua_gethookcount`
!!  * `lua_gethookmask`
!!  * `lua_getinfo`
!!  * `lua_getlocal`
!!  * `lua_getstack`
!!  * `lua_getupvalue`
!!  * `lua_sethook`
!!  * `lua_setlocal`
!!  * `lua_setupvalue`
!!  * `lua_upvalueid`
!!  * `lua_upvaluejoin`
!!
!! Auxiliary functions which are not inmplemented:
!!
!!  * luaL_addchar - lua_Buffer manipulation not supported
!!  * luaL_addlstring - lua_Buffer manipulation not supported
!!  * luaL_addsize - lua_Buffer manipulation not supported
!!  * luaL_addstring - lua_Buffer manipulation not supported
!!  * luaL_addvalue - lua_Buffer manipulation not supported
!!  * luaL_argcheck - error linking to C API function luaL_argerror - extern?
!!  * luaL_argerror - error linking to C API function luaL_argerror - extern?
!!  * luaL_buffinit - lua_Buffer manipulation not supported
!!  * luaL_buffinitsize - lua_Buffer manipulation not supported
!!  * luaL_checkoption - deals with arguments passed by Lua to an imported Fortran function
!!  * luaL_checkunsigned - deprecated in Lua 5.3
!!  * luaL_checkversion - C macro; underlying function may not be accessible. Also needs constants LUA_VERSION_NUM and LUAL_NUMSIZES
!!  * luaL_getmetatable - requires `LUA_REGISTRYINDEX` from `lua.h`; not exported
!!  * luaL_gsub - Basic text manipulation. Workaround: manipulate text in Fortran
!!  * luaL_loadbuffer - lua_Buffer manipulation not supported
!!  * luaL_loadbufferx - `lua_Buffer` manipulation not supported
!!  * luaL_newlib - `luaL_Reg` manipulation not supported
!!  * luaL_newlibtable - `luaL_Reg` manipulation not supported
!!  * luaL_opt - deals with arguments passed by Lua to an imported Fortran function
!!  * luaL_optinteger - deals with arguments passed by Lua to an imported Fortran function
!!  * luaL_optlstring - deals with arguments passed by Lua to an imported Fortran function
!!  * luaL_optnumber - deals with arguments passed by Lua to an imported Fortran function
!!  * luaL_optstring - deals with arguments passed by Lua to an imported Fortran function
!!  * luaL_optunsigned - deprecated in Lua 5.3
!!  * luaL_prepbuffer - lua_Buffer manipulation not supported
!!  * luaL_prepbuffsize - lua_Buffer manipulation not supported
!!  * luaL_pushresult - lua_Buffer manipulation not supported
!!  * luaL_pushresultsize - lua_Buffer manipulation not supported
!!  * luaL_requiref - direct manipulation of the package directory not supported in Fortran
!!  * luaL_setfuncs - `luaL_Reg` manipulation not supported
!!
!! See @cite Lua53Manual for details.
module lua
    use, intrinsic :: iso_fortran_env, only: INT64, REAL64
    use, intrinsic :: iso_c_binding
    implicit none
    private

    ! C API

    ! Unmplemented C API Types (* might be useful, o workaround available, X unsupported)
    !X lua_Alloc - not supported
    ! lua_CFunction - represented as c_funptr
    !* lua_Integer - represented as c_long_long -> INT64
    ! lua_KContext - numeric type of type intptr_t if available, ptrdiff_t otherwise
    ! lua_KFunction - represented as c_funptr
    !* lua_Number  - represented as c_double -> REAL64
    ! lua_Reader - represented as c_funptr
    ! lua_State - represented as c_ptr
    !X lua_Unsigned - deprecated
    !* lua_Writer - needed for lua_dump; likely needs a C implementation; see AOTUS

    ! Unmplemented C API Functions (* might be useful, o workaround available, X unsupported)
    !X lua_atpanic - panic functions not supported; requires C implementation
    !* lua_dump - requires lua_Writer which requires C implementation
    !X lua_getallocf - `lua_Alloc` not supported;  requires C implementation
    !X lua_getextraspace - requires `LUA_EXTRASPACE` from `lua.h`; not exported
    !o lua_newstate - `lua_Alloc` not supported; use `luaL_newstate`
    !o lua_pushfstring - compose string with format() then use `lua_pushstring`
    !* lua_pushglobaltable - requires `LUA_REGISTRYINDEX` and `LUA_RIDX_GLOBALS` (= 2) from `lua.h`; not exported.
    !o lua_pushvfstring - compose string with format() then use `lua_pushstring`
    !X lua_setallocf - `lua_Alloc` not supported

    ! Debug Interface (not implemented)

    ! Unimplemented Debug types
    !X lua_Debug
    !X lua_Hook

    ! Unimplemented Debug functions
    !X lua_gethook
    !X lua_gethookcount
    !X lua_gethookmask
    !X lua_getinfo
    !X lua_getlocal
    !X lua_getstack
    !X lua_getupvalue
    !X lua_sethook
    !X lua_setlocal
    !X lua_setupvalue
    !X lua_upvalueid
    !X lua_upvaluejoin

    ! Auxiliary Library

    ! Unimplemented Auxiliary Library types
    ! luaL_Buffer
    ! luaL_Reg
    ! luaL_Stream

    ! Unimplemented Auxiliary Library functions (* considered important)
    ! luaL_addchar - buffer operation
    ! luaL_addlstring - buffer operation
    ! luaL_addsize - buffer operation
    ! luaL_addstring - buffer operation
    ! luaL_addvalue - buffer operation
    !M luaL_argcheck - error linking to C API function luaL_argerror - extern?
    ! luaL_argerror - error linking to C API function luaL_argerror - extern?
    ! luaL_buffinit - buffer operation
    ! luaL_buffinitsize - buffer operation
    ! luaL_checkoption - deals with arguments passed by Lua to an imported Fortran function
    !M luaL_getmetatable - requires `LUA_REGISTRYINDEX` from `lua.h`; not exported
    ! luaL_gsub - text manipulation. Workaround: Manipulate text in Fortran
    !M luaL_loadbuffer - buffer operation
    ! luaL_loadbufferx - buffer operation
    !M luaL_newlib
    !M luaL_newlibtable
    !M luaL_opt - deals with arguments passed by Lua to an imported Fortran function
    ! luaL_optinteger - deals with arguments passed by Lua to an imported Fortran function
    ! luaL_optlstring - deals with arguments passed by Lua to an imported Fortran function
    ! luaL_optnumber - deals with arguments passed by Lua to an imported Fortran function
    !M luaL_optstring - deals with arguments passed by Lua to an imported Fortran function
    ! luaL_prepbuffer - buffer operation
    ! luaL_prepbuffsize - buffer operation
    ! luaL_pushresult - buffer operation
    ! luaL_pushresultsize - buffer operation
    ! luaL_requiref
    ! luaL_setfuncs

    ! Equivalent 'kinds' of Lua values

    !> Fortran integer `kind` mapping to lua_Integer
    integer, parameter, public :: c_lua_integer = c_long_long

    !> Fortran real `kind` mapping to lua_Number
    integer, parameter, public :: c_lua_number = c_double

    ! Implemented C API Functions
    public :: lua_absindex
    public :: lua_arith
    ! public :: lua_atpanic - demonstrate need - low-level process signalling beyond scope
    public :: lua_call
    public :: lua_callk
    public :: lua_checkstack
    public :: lua_close
    public :: lua_compare
    public :: lua_concat
    public :: lua_copy
    public :: lua_createtable
    !* public :: lua_dump - demonstrate need; complex and requires C coding
    public :: lua_error
    public :: lua_gc
    ! public :: lua_getallocf - demonstrate need - low-level memory management beyond scope
    ! public :: lua_getextraspace ! - demonstrate need - low-level memory management beyond scope
    public :: lua_getfield
    public :: lua_getglobal
    public :: lua_geti
    public :: lua_getmetatable
    public :: lua_gettable
    public :: lua_gettop
    public :: lua_getuservalue
    public :: lua_insert
    public :: lua_isboolean
    public :: lua_iscfunction
    public :: lua_isfunction
    public :: lua_isinteger
    public :: lua_isnil
    public :: lua_isnone
    public :: lua_isnoneornil
    public :: lua_isnumber
    public :: lua_isstring
    public :: lua_istable
    public :: lua_isthread
    public :: lua_isuserdata
    public :: lua_isyieldable
    public :: lua_len
    public :: lua_load
    ! public :: lua_newstate - use lual_newstate
    public :: lua_newtable
    public :: lua_newthread
    public :: lua_newuserdata
    public :: lua_next
    public :: lua_numbertointeger
    public :: lua_pcall
    public :: lua_pcallk
    public :: lua_pop
    public :: lua_pushboolean
    public :: lua_pushcclosure
    ! public :: lua_pushfstring - compose string with format() then use lua_pushstring
    ! public :: lua_pushglobaltable ! requires `LUA_REGISTRYINDEX` and `LUA_RIDX_GLOBALS` (= 2) from `lua.h`; not exported.
    public :: lua_pushinteger
    public :: lua_pushlightuserdata
    public :: lua_pushliteral
    public :: lua_pushlstring
    public :: lua_pushnil
    public :: lua_pushnumber
    public :: lua_pushstring
    public :: lua_pushthread
    public :: lua_pushvalue
    ! public :: lua_pushvfstring - compose string with format() then use lua_pushstring
    public :: lua_rawequal
    public :: lua_rawget
    public :: lua_rawgeti
    public :: lua_rawgetp
    public :: lua_rawlen
    public :: lua_rawset
    public :: lua_rawseti
    public :: lua_rawsetp
    public :: lua_register
    public :: lua_remove
    public :: lua_replace
    public :: lua_resume
    public :: lua_rotate
    ! public :: lua_setallocf - demonstrate need - low-level memory management beyond scope
    public :: lua_setfield
    public :: lua_setglobal
    public :: lua_seti
    public :: lua_setmetatable
    public :: lua_settable
    public :: lua_settop
    public :: lua_setuservalue
    public :: lua_status
    public :: lua_stringtonumber
    public :: lua_toboolean
    public :: lua_tocfunction
    public :: lua_tointeger
    public :: lua_tointegerx
    public :: lua_tonumber
    public :: lua_tonumberx
    public :: lua_topointer
    public :: lua_tostring
    public :: lua_tothread
    public :: lua_touserdata
    public :: lua_type
    public :: lua_typename
    ! public :: lua_upvalueindex - requires `LUA_REGISTRYINDEX` from `lua.h`; not exported
    public :: lua_version
    public :: lua_xmove
    public :: lua_yield
    public :: lua_yieldk

    ! Debug interface functions
    ! public :: lua_gethook
    ! public :: lua_gethookcount
    ! public :: lua_gethookmask
    ! public :: lua_getinfo
    ! public :: lua_getlocal
    ! public :: lua_getstack
    ! public :: lua_getupvalue
    ! public :: lua_sethook
    ! public :: lua_setlocal
    ! public :: lua_setupvalue
    ! public :: lua_upvalueid
    ! public :: lua_upvaluejoin

    ! Auxiliary Library functions
    ! luaL_addchar ! Buffer operation not supported
    ! luaL_addlstring ! Buffer operation not supported
    ! luaL_addsize ! Buffer operation not supported
    ! luaL_addstring ! Buffer operation not supported
    ! luaL_addvalue ! Buffer operation not supported
    ! public :: lual_argcheck - error linking to C API function luaL_argerror - extern?
    ! public :: lual_argerror - error linking to C API function luaL_argerror - extern?
    ! luaL_buffinit ! Buffer operation not supported
    ! luaL_buffinitsize ! Buffer operation not supported
    public :: lual_callmeta
    public :: lual_checkany
    public :: lual_checkinteger
    public :: lual_checklstring
    public :: lual_checknumber
    ! luaL_checkoption
    public :: lual_checkstack
    public :: lual_checkstring
    public :: lual_checktype
    public :: lual_checkudata
    ! public :: luaL_checkversion ! C macro, needs constants and luaL_checkversion_ which may not be available
    public :: lual_dofile
    public :: lual_dostring
    public :: lual_error
    public :: lual_execresult
    public :: lual_fileresult
    public :: lual_getmetafield
    ! luaL_getmetatable - requires `LUA_REGISTRYINDEX` from `lua.h`; not exported
    public :: lual_getsubtable
    ! luaL_gsub - Basic text manipulation. Workaround: manipulate text in Fortran
    ! luaL_loadbuffer ! Buffer operation not supported
    ! luaL_loadbufferx ! Buffer operation not supported
    public :: lual_loadfile
    public :: lual_loadfilex
    public :: lual_loadstring
    ! luaL_newlib
    ! luaL_newlibtable
    public :: lual_newmetatable
    public :: lual_newstate
    public :: lual_openlibs
    ! luaL_opt
    ! luaL_optinteger
    ! luaL_optlstring
    ! luaL_optnumber
    ! luaL_optstring
    ! luaL_prepbuffer ! Buffer operation not supported
    ! luaL_prepbuffsize ! Buffer operation not supported
    ! luaL_pushresult ! Buffer operation not supported
    ! luaL_pushresultsize ! Buffer operation not supported
    public :: lual_ref
    ! luaL_requiref ! Access to `require` and package directory is not supported in Fortran
    ! luaL_setfuncs ! lua_Reg operations not supported
    public :: luaL_setmetatable
    public :: lual_testudata
    public :: lual_tolstring
    public :: lual_traceback
    public :: lual_typename
    public :: lual_unref
    public :: lual_where

    ! Internal debugging
    ! public :: c_strlen
    ! public :: lua_tolstring

    !> @name Thread control options

    !!!@{
    !> Option for multiple returns in `lua_pcall()` and `lua_call()`.
    integer(kind=c_int), parameter, public :: LUA_TH_MULTRET = -1
    !!!@}

    ! Basic types.

    !> @name Basic Lua types

    !!!@{
    !> Undefined Lua type
    integer(kind=c_int), parameter, public :: LUA_TNONE          = -1
    !> Lua `nil` type
    integer(kind=c_int), parameter, public :: LUA_TNIL           = 0
    !> Lua `boolean` type
    integer(kind=c_int), parameter, public :: LUA_TBOOLEAN       = 1
    !> Lua light `userdata` type (low-cost wrapper around a C pointer,
    !! has no metatables, and is not garbage-collected)
    integer(kind=c_int), parameter, public :: LUA_TLIGHTUSERDATA = 2
    !> Lua `number` type
    integer(kind=c_int), parameter, public :: LUA_TNUMBER        = 3
    !> Lua `string` type
    integer(kind=c_int), parameter, public :: LUA_TSTRING        = 4
    !> Lua `table` type
    integer(kind=c_int), parameter, public :: LUA_TTABLE         = 5
    !> Lua `function` type
    integer(kind=c_int), parameter, public :: LUA_TFUNCTION      = 6
    !> Lua full `userdata` type
    integer(kind=c_int), parameter, public :: LUA_TUSERDATA      = 7
    !> Lua `thread` type
    integer(kind=c_int), parameter, public :: LUA_TTHREAD        = 8
    !!!@}

    !> @name Arithmetic and bitwise operator codes

    !!!@{
    !> Lua arithmetic addition operator `+`
    integer(kind=c_int), parameter, public :: LUA_OPADD  = 0
    !> Lua arithmetic subtraction operator `-`
    integer(kind=c_int), parameter, public :: LUA_OPSUB  = 1
    !> Lua arithmetic multiplication operator `*`
    integer(kind=c_int), parameter, public :: LUA_OPMUL  = 2
    !> Lua arithmetic modulo operator `%`
    integer(kind=c_int), parameter, public :: LUA_OPMOD  = 3
    !> Lua arithmetic exponentiation operator `^`
    integer(kind=c_int), parameter, public :: LUA_OPPOW  = 4
    !> Lua arithmetic division operator `/`
    integer(kind=c_int), parameter, public :: LUA_OPDIV  = 5
    !> Lua arithmetic 'floor division' operator `//`
    integer(kind=c_int), parameter, public :: LUA_OPIDIV = 6
    !> Lua bitwise-AND operator `&`
    integer(kind=c_int), parameter, public :: LUA_OPBAND = 7
    !> Lua bitwise-OR operator `|`
    integer(kind=c_int), parameter, public :: LUA_OPBOR  = 8
    !> Lua bitwise-XOR operator '~'
    integer(kind=c_int), parameter, public :: LUA_OPBXOR = 9
    !> Lua bitwise left shift operator `<<`
    integer(kind=c_int), parameter, public :: LUA_OPSHL  = 10
    !> Lua bitwise right shift operator `>>`
    integer(kind=c_int), parameter, public :: LUA_OPSHR  = 11
    !> Lua arithmetic unary minus operator `-`
    integer(kind=c_int), parameter, public :: LUA_OPUNM  = 12
    !> Lua bitwise-NOT operator `~`
    integer(kind=c_int), parameter, public :: LUA_OPBNOT = 13
    !!!@}

    !> @name Comparison operator codes

    !!!@{
    !> Lua relational operator `==`
    integer(kind=c_int), parameter, public :: LUA_OPEQ = 0
    !> Lua relational operator `<`
    integer(kind=c_int), parameter, public :: LUA_OPLT = 1
    !> Lua relational operator `<=`
    integer(kind=c_int), parameter, public :: LUA_OPLE = 2
    !!!@}

    !> @name Garbage-collection options.

    !!!@{
    !> Garbage collection control: Stops the garbage collector
    integer(kind=c_int), parameter, public :: LUA_GCSTOP       = 0
    !> Garbage collection control: Restarts the garbage collector
    integer(kind=c_int), parameter, public :: LUA_GCRESTART    = 1
    !> Garbage collection control: Performs a full garbage-collection
    !! cycle
    integer(kind=c_int), parameter, public :: LUA_GCCOLLECT    = 2
    !> Garbage collection control: Returns the current amount of memory
    !! (in Kbytes) in use by Lua
    integer(kind=c_int), parameter, public :: LUA_GCCOUNT      = 3
    !> Garbage collection control: Returns the remainder of dividing
    !! the current amount of bytes of memory in use by Lua by 1024
    integer(kind=c_int), parameter, public :: LUA_GCCOUNTB     = 4
    !> Garbage collection control: Performs an incremental step of
    !! garbage collection
    integer(kind=c_int), parameter, public :: LUA_GCSTEP       = 5
    !> Garbage collection control: Sets data as the new value for the
    !! pause of the collector and returns the previous value of the
    !! pause
    integer(kind=c_int), parameter, public :: LUA_GCSETPAUSE   = 6
    !> Garbage collection control: Sets data as the new value for the
    !! step multiplier of the collector and returns the previous value
    !! of the step multiplier
    integer(kind=c_int), parameter, public :: LUA_GCSETSTEPMUL = 7
    !> Garbage collection control: Returns a boolean that tells whether
    !! the collector is running (i.e., not stopped)
    integer(kind=c_int), parameter, public :: LUA_GCISRUNNING  = 9
    !!!@}

    !> @name Thread status.

    !!!@{
    !> Thread status: (from `lua_pcall`) success, (from `lua_resume`)
    !! indicates the coroutine finished its execution without errors.
    !! Renamed from `LUA_OK` to `LUA_TH_OK`
    !! (function `lua_yield()` name collides with `LUA_YIELD`)
    integer(kind=c_int), parameter, public :: LUA_TH_OK        = 0
    !> Thread status: (from `lua_resume`) Indicates that the coroutine
    !! yields, (from `lua_status`) a thread is suspended
    !! Renamed from `LUA_YIELD` to `LUA_TH_YIELD`
    !! (function `lua_yield()` name collides with `LUA_YIELD`)
    integer(kind=c_int), parameter, public :: LUA_TH_YIELD     = 1
    !> Thread status: (from `pcall`) a runtime error
    !! Renamed from `LUA_ERRRUN` to `LUA_TH_ERRRUN`
    !! (function `lua_yield()` name collides with `LUA_YIELD`)
    integer(kind=c_int), parameter, public :: LUA_TH_ERRRUN    = 2
    !> Thread status: (from `lua_load`) syntax error during
    !! precompilation
    !! Renamed from `LUA_ERRSYNTAX` to `LUA_TH_ERRSYNTAX`
    !! (function `lua_yield()` name collides with `LUA_YIELD`)
    integer(kind=c_int), parameter, public :: LUA_TH_ERRSYNTAX = 3
    !> Thread status: (from `lua_pcall`, `lua_load`) memory allocation
    !! error. For such errors resulting from `lua_pcall`, Lua does not
    !! call the message handler.
    !! Renamed from `LUA_ERRMEM` to `LUA_TH_ERRMEM`
    !! (function `lua_yield()` name collides with `LUA_YIELD`)
    integer(kind=c_int), parameter, public :: LUA_TH_ERRMEM    = 4
    !> Thread status: (from `lua_pcall`) error while running a
    !! `__gc` metamethod. For such errors, Lua does not call the message
    !! handler (as this kind of error typically has no relation with the
    !! function being called).
    !! Renamed from `LUA_ERRGCMM` to `LUA_TH_ERRGCMM`
    !! (function `lua_yield()` name collides with `LUA_YIELD`)
    integer(kind=c_int), parameter, public :: LUA_TH_ERRGCMM   = 5
    !> Thread status: (from `lua_pcall`) error while running the message
    !! handler
    !! Renamed from `LUA_ERRERR` to `LUA_TH_ERRERR` (function
    !! `lua_yield()` name collides with `LUA_YIELD`)
    integer(kind=c_int), parameter, public :: LUA_TH_ERRERR    = 6
    !!!@}

    !> @name `libc` function interface

    !!!@{
    interface
        !> Wrapper around C `strlen` function
        function c_strlen(str) bind(c, name='strlen')
            import :: c_ptr, c_size_t
            !> Pointer to string
            type(c_ptr), intent(in), value :: str
            ! Return value, string length
            integer(c_size_t)              :: c_strlen
        end function c_strlen
    end interface
    !!!@}

    !> @name Lua 5.3 function interface

    !!!@{
    interface

        !> @brief Converts the acceptable index idx into an equivalent
        !! absolute index (that is, one that does not depend on the
        !! stack top)
        !!
        !! C signature: `int lua_absindex(lua_State *L, int idx)`
        function lua_absindex(l, idx) bind(c, name='lua_absindex')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to test
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)                    :: lua_absindex
        end function lua_absindex

        !> @brief Ensures that the stack has space for at least *n* extra
        !! slots (that is, that you can safely push up to *n* values into
        !! it).
        !!
        !! It returns false if it cannot fulfill the request, either
        !! because it would cause the stack to be larger than a fixed
        !! maximum size (typically at least several thousand elements)
        !! or because it cannot allocate memory for the extra space.
        !! This function never shrinks the stack; if the stack already
        !! has space for the extra slots, it is left unchanged.
        !!
        !! C signature: `int lua_checkstack(lua_State *L, int n)`
        function lua_checkstack(l, n) bind(c, name='lua_checkstack')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Desired number of stack entries
            integer(kind=c_int), intent(in), value :: n
            ! Return value
            integer(kind=c_int)                    :: lua_checkstack
        end function lua_checkstack

        !> @brief Compares two Lua values.
        !!
        !! Returns 1 if the value at index
        !! `index1` satisfies op when compared with the value at index
        !! `index2`, following the semantics of the corresponding Lua
        !! operator (that is, it may call metamethods). Otherwise
        !! returns 0. Also returns 0 if any of the indices is not valid.
        !!
        !! The value of op must be one of the following constants:
        !!  * `LUA_OPEQ`: compares for equality (`==`)
        !!  * `LUA_OPLT`: compares for less than (`<`)
        !!  * `LUA_OPLE`: compares for less or equal (`<=`)
        !!
        !! C signature: `int lua_compare_(lua_State *L, int index1, int index2, int op)`
        function lua_compare_(l, index1, index2, op) bind(c, name='lua_compare')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of first value in comparison
            integer(kind=c_int), intent(in), value :: index1
            !> Index of second value in comparison
            integer(kind=c_int), intent(in), value :: index2
            !> Comparison operator (one of `LUA_OPEQ`, `LUA_OPLT`, or `LUA_OPLE`)
            integer(kind=c_int), intent(in), value :: op
            ! Return value
            integer(kind=c_int)                    :: lua_compare_
        end function lua_compare_

        !> @brief Generates a Lua error, using the value at the top of
        !! the stack as the error object.
        !!
        !! This function does a long jump, and therefore never returns
        !! (see `luaL_error`)
        !!
        !! C signature: `int lua_error(lua_State *L)`
        function lua_error(l) bind(c, name='lua_error')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            ! Return value
            integer(kind=c_int)                    :: lua_error
        end function lua_error

        !> @brief Controls the garbage collector.
        !!
        !! This function performs several tasks, according to the value
        !! of the parameter `what`:
        !!
        !!    * `LUA_GCSTOP`: stops the garbage collector.
        !!    * `LUA_GCRESTART`: restarts the garbage collector.
        !!    * `LUA_GCCOLLECT`: performs a full garbage-collection cycle.
        !!    * `LUA_GCCOUNT`: returns the current amount of memory (in Kbytes) in use by Lua.
        !!    * `LUA_GCCOUNTB`: returns the remainder of dividing the current amount of bytes of memory in use by Lua by 1024.
        !!    * `LUA_GCSTEP`: performs an incremental step of garbage collection.
        !!    * `LUA_GCSETPAUSE`: sets `data` as the new value for the pause of the collector; function returns the previous value of the pause.
        !!    * `LUA_GCSETSTEPMUL`: sets `data` as the new value for the step multiplier of the collector; function returns the previous value of the step multiplier.
        !!    * `LUA_GCISRUNNING`: returns a boolean that tells whether the collector is running (i.e., not stopped).
        !!
        !! C signature: `int lua_gc(lua_State *L, int what, int data)`
        function lua_gc(l, what, data) bind(c, name='lua_gc')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Directive to send to garbage collector
            integer(kind=c_int), intent(in), value :: what
            !> Data for setting the garbage collector's step multiplier or pause interval
            integer(kind=c_int), intent(in), value :: data
            ! Return value
            integer(kind=c_int)                    :: lua_gc
        end function lua_gc

        !> @brief Pushes onto the stack the value of the global name.
        !! Returns the type of that value.
        !!
        !! This C API function is shadowed by the Fortran wrapper
        !! function `lua_getglobal()` which automatically terminates the
        !! Fortran string with `\0` for C compatibility. Do not use this
        !! function in Fortran code unless you really know what you are
        !! doing.
        !!
        !! C signature: `int lua_getglobal(lua_State *L, const char *name)`
        function lua_getglobal_(l, name) bind(c, name='lua_getglobal')
            import :: c_char, c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Name of element to push onto the stack
            character(kind=c_char), intent(in)        :: name
            ! Return value
            integer(kind=c_int)                       :: lua_getglobal_
        end function lua_getglobal_

        !> @brief Pushes onto the stack the value `t[k]`, where `t` is the
        !! value (table) at the given index. Returns the type of the pushed
        !! value (`t[k]`).
        !!
        !! As in Lua, this function may trigger a metamethod for the
        !! "index" event.
        !!
        !! This C API function is shadowed by the Fortran wrapper
        !! function `lua_getfield()` which automatically terminates the
        !! Fortran string (`name`) with `\0` for C compatibility. Do not
        !! use this function in Fortran code unless you really know what
        !! you are doing.
        !!
        !! C signature: `int lua_getfield(lua_State *L, int index, const char *name)`
        function lua_getfield_(l, idx, name) bind(c, name='lua_getfield')
            import :: c_char, c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of table
            integer(kind=c_int),    intent(in), value :: idx
            !> Name of field to push onto the stack
            character(kind=c_char), intent(in)        :: name
            ! Return value
            integer(kind=c_int)                       :: lua_getfield_
        end function lua_getfield_

        !> @brief Pushes onto the stack the value of the indexed table
        !! element `t[i]`. Returns the type of that value.
        !!
        !! C signature: `int lua_geti (lua_State *L, int index, lua_Integer i)`
        function lua_geti(l, idx, i) bind(c, name='lua_geti')
            import :: c_int, c_ptr, c_lua_integer
            !> Pointer to Lua interpreter state
            type(c_ptr),               intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int),       intent(in), value :: idx
            !> Index of value in table
            integer(kind=c_lua_integer), intent(in), value :: i
            ! Return value
            integer(kind=c_int)                          :: lua_geti
        end function lua_geti

        !> @brief If the value at the given index has a metatable, the
        !! function pushes that metatable onto the stack and returns 1.
        !! Otherwise, the function returns 0 and pushes nothing on the
        !! stack.
        !!
        !! C signature: `int lua_getmetatable (lua_State *L, int index)`
        function lua_getmetatable(l, idx) bind(c, name='lua_getmetatable')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of element from which the metatable will be extracted
            integer(kind=c_int), intent(in), value    :: idx
            ! Return value
            integer(kind=c_int)                       :: lua_getmetatable
        end function lua_getmetatable

        !> @brief Pushes onto the stack the value `t[k]`, where `t` is
        !! the value at the given index (a table) and `k` is the value
        !! at the top of the stack (key).
        !!
        !! Returns the type of the pushed value.
        !!
        !! This function pops the key from the stack, pushing the
        !! resulting value in its place. As in Lua, this function may
        !! trigger a metamethod for the "index" event.
        !!
        !! C signature: `int lua_gettable (lua_State *L, int index)`
        function lua_gettable(l, idx) bind(c, name='lua_gettable')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int), intent(in), value    :: idx
            ! Return value
            integer(kind=c_int)                       :: lua_gettable
        end function lua_gettable

        !> @brief Returns the index of the top element in the stack.
        !!
        !! Because indices start at 1, this result is equal to the
        !! number of elements in the stack; in particular, 0 means an
        !! empty stack.
        !!
        !! C signature: `int lua_gettop(lua_State *L)`
        function lua_gettop(l) bind(c, name='lua_gettop')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
            ! Return value
            integer(kind=c_int)            :: lua_gettop
        end function lua_gettop

        !> @brief Pushes onto the stack the Lua value associated with
        !! the full userdata at the given index.
        !!
        !! Returns the type of the pushed value.
        !!
        !! C signature: `int lua_getuservalue (lua_State *L, int index)`
        function lua_getuservalue(l, idx) bind(c, name='lua_getuservalue')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of userdata on stack
            integer(kind=c_int), intent(in), value    :: idx
            ! Return value
            integer(kind=c_int)                       :: lua_getuservalue
        end function lua_getuservalue

        ! !***** Why is this failing to link? MACRO!!!

        ! !> @brief Moves the top element into the given valid index,
        ! !! shifting up the elements above this index to open space.
        ! !!
        ! !! This function cannot be called with a pseudo-index, because
        ! !! a pseudo-index is not an actual stack position.
        ! !!
        ! !! C signature: `void lua_insert (lua_State *L, int index)`
        ! subroutine lua_insert(l, idx) bind(c, name='lua_insert')
        !     import :: c_int, c_ptr
        !     !> Pointer to Lua interpreter state
        !     type(c_ptr),            intent(in), value :: l
        !     !> Index of table on stack
        !     integer(kind=c_int), intent(in), value    :: idx
        ! end subroutine lua_insert

        !> @brief Returns 1 if the value at the given index is a
        !! C function, and 0 otherwise.
        !!
        !! C signature: `int lua_iscfunction(lua_State *L, int idx)`
        function lua_iscfunction_(l, idx) bind(c, name='lua_iscfunction')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to test
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)                    :: lua_iscfunction_
        end function lua_iscfunction_

        !> @brief Returns 1 if the value at the given index is an
        !! integer (that is, the value is a number and is represented
        !! as an integer), and 0 otherwise.
        !!
        !! C signature: `int lua_isinteger(lua_State *L, int idx)`
        function lua_isinteger_(l, idx) bind(c, name='lua_isinteger')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to test
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)                    :: lua_isinteger_
        end function lua_isinteger_

        !> @brief Returns 1 if the value at the given index is a number
        !! or a string convertible to a number, and 0 otherwise.
        !!
        !! C signature: `int lua_isnumber(lua_State *L, int idx)`
        function lua_isnumber_(l, idx) bind(c, name='lua_isnumber')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to test
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)                    :: lua_isnumber_
        end function lua_isnumber_

        !> @brief Returns 1 if the value at the given index is a string
        !! or a number (which is always convertible to a string), and 0
        !! otherwise.
        !!
        !! C signature: `int lua_isstring(lua_State *L, int idx)`
        function lua_isstring_(l, idx) bind(c, name='lua_isstring')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to test
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)                    :: lua_isstring_
        end function lua_isstring_

        !> @brief Returns 1 if the value at the given index is a
        !! userdata (either full or light), and 0 otherwise.
        !!
        !! C signature: `int lua_isuserdata(lua_State *L, int idx)`
        function lua_isuserdata_(l, idx) bind(c, name='lua_isuserdata')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to test
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)                    :: lua_isuserdata_
        end function lua_isuserdata_

        !> @brief Returns 1 if the given coroutine can yield, and 0
        !! otherwise.
        !!
        !! C signature: `int lua_isyieldable(lua_State *L)`
        function lua_isyieldable_(l) bind(c, name='lua_isyieldable')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
            ! Return value
            integer(kind=c_int)            :: lua_isyieldable_
        end function lua_isyieldable_

        !> @brief Returns the length of the value at the given index.
        !!
        !! It is equivalent to the '#' operator in Lua and may trigger a
        !! metamethod for the "length" event. The result is pushed on
        !! the stack.
        !!
        !! C signature: `void lua_len (lua_State *L, int index)`
        subroutine lua_len(l, idx) bind(c, name='lua_len')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to test
            integer(kind=c_int), intent(in), value :: idx
        end subroutine lua_len

        !> @brief Loads a Lua chunk without running it.
        !!
        !! If there are no errors, `lua_load` pushes the compiled chunk
        !! as a Lua function on top of the stack. Otherwise, it pushes
        !! an error message.
        !!
        !! The return values of `lua_load` are:
        !!   * `LUA_TH_OK`: no errors;
        !!   * `LUA_TH_ERRSYNTAX`: syntax error during precompilation;
        !!   * `LUA_TH_ERRMEM`: memory allocation (out-of-memory) error;
        !!   * `LUA_TH_ERRGCMM`: error while running a `__gc` metamethod.
        !!     (This error has no relation with the chunk being loaded.
        !!     It is generated by the garbage collector.)
        !!
        !! The `lua_load` function uses a user-supplied reader function
        !! to read the chunk (see `lua_Reader`). The `data` argument is
        !! an opaque value passed to the reader function.
        !!
        !! The chunkname argument gives a name to the chunk, which is
        !! used for error messages and in debug information.
        !!
        !! `lua_load` automatically detects whether the chunk is text or
        !! binary and loads it accordingly (see program `luac`). The
        !! string `mode` works as in function `load`, with the addition
        !! that a `NULL` value is equivalent to the string "bt".
        !!
        !! `lua_load` uses the stack internally, so the reader function
        !! must always leave the stack unmodified when returning.
        !!
        !! If the resulting function has upvalues, its first upvalue is
        !! set to the value of the global environment stored at index
        !! `LUA_RIDX_GLOBALS` in the registry. When loading main chunks,
        !! this upvalue will be the `_ENV` variable. Other upvalues are
        !! initialized with `nil`.
        !!
        !! C signature: `int lua_load(lua_State *L, lua_Reader reader, void *data, const char *chunkname, const char *mode)`
        function lua_load(l, reader, data, chunkname, mode) bind(c, name='lua_load')
            import :: c_char, c_funptr, c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Pointer to a user-supplied reader function to read the
            !! chunk (see `lua_Reader`)
            type(c_funptr),         intent(in), value :: reader
            !> An opaque value passed to the reader function
            type(c_ptr),            intent(in), value :: data
            !> Name for the chunk, used for error messages and in debug
            !! information
            character(kind=c_char), intent(in)        :: chunkname
            !> Binary/text chunk mode. `mode` works as in function
            !! `load`, with the addition that a `NULL` value is
            !! equivalent to the string "bt".
            character(kind=c_char), intent(in)        :: mode
            ! Return value
            integer(kind=c_int)                       :: lua_load
        end function lua_load

        !> @brief Creates a new thread, pushes it on the stack, and
        !! returns a pointer to a `lua_State` that represents this new
        !! thread.
        !!
        !! The new thread returned by this function shares with the
        !! original thread its global environment, but has an
        !! independent execution stack.
        !!
        !! There is no explicit function to close or to destroy a thread.
        !! Threads are subject to garbage collection, like any Lua object.
        !!
        !! C signature: `lua_State *lua_newthread (lua_State *L)`
        function lua_newthread(l) bind(c, name='lua_newthread')
            import :: c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
            ! Return value
            type(c_ptr)                    :: lua_newthread
        end function lua_newthread

        !> @brief This function allocates a new block of memory with the
        !! given size, pushes onto the stack a new full userdata with the
        !! block address, and returns this address.
        !!
        !! The host program can freely use this memory.
        !!
        !! C signature: `void *lua_newuserdata (lua_State *L, size_t size)`
        function lua_newuserdata(l, size) bind(c, name='lua_newuserdata')
            import :: c_int, c_ptr, c_size_t
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Size of userdata memory block
            integer(kind=c_size_t), intent(in), value :: size

            ! Return value
            type(c_ptr)                               :: lua_newuserdata
        end function lua_newuserdata

        !> @brief Pops a key from the stack, and pushes a key–value pair
        !! from the table at the given index (the "next" pair after the
        !! given key). If there are no more elements in the table, then
        !! `lua_next` returns 0 (and pushes nothing).
        !!
        !! A typical traversal looks like this:
        !!
        !! @code{.c}
        !! /* table is in the stack at index 't' */
        !! lua_pushnil(L);  /* first key */
        !! while (lua_next(L, t) != 0) {
        !!   /* uses 'key' (at index -2) and 'value' (at index -1) */
        !!   printf("%s - %s\n",
        !!          lua_typename(L, lua_type(L, -2)),
        !!          lua_typename(L, lua_type(L, -1)));
        !!   /* removes 'value'; keeps 'key' for next iteration */
        !!   lua_pop(L, 1);
        !! }
        !! @endcode
        !!
        !! or
        !!
        !! @code{.f90}
        !! ! Table is in the stack at index 't'
        !! ! Push nil to stack which tells lua_next() to get the first
        !! ! key-value pair
        !! call lua_pushnil(L)
        !! do while (lua_next(L, t) /= 0)
        !!   ! Uses 'key' (at index -2) and 'value' (at index -1)
        !!   write(unit=output_unit, format='(A, " - ", A)')         &
        !!          lua_typename(L, lua_type(L, -2)),                &
        !!          lua_typename(L, lua_type(L, -1)))
        !!   ! removes 'value'; keeps 'key' for next iteration
        !!   call lua_pop(L, 1)
        !! end do
        !! @endcode
        !!
        !! While traversing a table, do not call `lua_tolstring`
        !! directly on a key, unless you know that the key is actually a
        !! string. Recall that `lua_tolstring` may change the value at
        !! the given index; this confuses the next call to `lua_next`.
        !!
        !! See the Lua function `next` for the caveats of modifying the
        !! table during its traversal.
        !!
        !! C signature: `int lua_next (lua_State *L, int index)`
        function lua_next(l, idx) bind(c, name='lua_next')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of previous key extracted from table; set to `nil`
            !! to extract first key
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)                    :: lua_next
        end function lua_next

        !> @brief  Starts and resumes a coroutine in the given thread `L`.
        !!
        !! To start a coroutine, you push onto the thread stack the main
        !! function plus any arguments; then you call `lua_resume`, with
        !! `nargs` being the number of arguments. This call returns when
        !! the coroutine suspends or finishes its execution. When it
        !! returns, the stack contains all values passed to `lua_yield`,
        !! or all values returned by the body function. `lua_resume`
        !! returns `LUA_TH_YIELD` if the coroutine yields, `LUA_TH_OK` if the
        !! coroutine finishes its execution without errors, or an error
        !! code in case of errors (see `lua_pcall`).
        !!
        !! In case of errors, the stack is not unwound, so you can use
        !! the debug API over it. The error object is on the top of the
        !! stack.
        !!
        !! To resume a coroutine, you remove any results from the last
        !! `lua_yield`, put on its stack only the values to be passed as
        !! results from `yield`, and then call `lua_resume`.
        !!
        !! The parameter `from` represents the coroutine that is
        !! resuming `L`. If there is no such coroutine, this parameter
        !! can be `NULL`.
        !!
        !! C signature: `int lua_resume (lua_State *L, lua_State *from, int nargs)`
        function lua_resume(l, from, nargs) bind(c, name='lua_resume')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Pointer to coroutine (may be NULL)
            type(c_ptr),            intent(in), value :: from
            !> Index of table
            integer(kind=c_int),    intent(in), value :: nargs

            ! Return value
            integer(kind=c_int)                       :: lua_resume
        end function lua_resume

        !> @brief Does the equivalent to `t[k] = v`, where `t` is the
        !! value (table) at the given index and `v` is the value at the
        !! top of the stack.
        !!
        !! This function pops the value from the stack. As in Lua, this
        !! function may trigger a metamethod for the "newindex" event
        !!
        !! This C API function is shadowed by the Fortran wrapper
        !! function `lua_setfield()` which automatically terminates the
        !! Fortran string (`name`) with `\0` for C compatibility. Do not
        !! use this function in Fortran code unless you really know what
        !! you are doing.
        !!
        !! C signature: `void lua_setfield (lua_State *L, int index, const char *k);`
        subroutine lua_setfield_(l, idx, name) bind(c, name='lua_setfield')
            import :: c_char, c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of table
            integer(kind=c_int),    intent(in), value :: idx
            !> Name of field (key) to push onto the stack
            character(kind=c_char), intent(in)        :: name
        end subroutine lua_setfield_

        !> @brief Does the equivalent to `t[n] = v`, where `t` is the
        !! value at the given index and `v` is the value at the top of
        !! the stack.
        !!
        !! This function pops the value from the stack. As in Lua, this
        !! function may trigger a metamethod for the "newindex" event
        !!
        !! C signature: `void lua_seti (lua_State *L, int index, lua_Integer n)`
        subroutine lua_seti(l, idx, n) bind(c, name='lua_seti')
            import :: c_int, c_ptr, c_lua_integer
            !> Pointer to Lua interpreter state
            type(c_ptr),             intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int), intent(in), value     :: idx
            !> Index of value in table
            integer(kind=c_lua_integer), intent(in), value :: n
        end subroutine lua_seti

        !> @brief Pops a table from the stack and sets it as the new
        !! metatable for the value at the given index.
        !!
        !! C signature: `void lua_setmetatable (lua_State *L, int index)`
        subroutine lua_setmetatable(l, idx) bind(c, name='lua_setmetatable')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of stack to associate with metatable
            integer(kind=c_int), intent(in), value    :: idx
        end subroutine lua_setmetatable

        !> @brief Does the equivalent to t[k] = v, where t is the value
        !! at the given index, v is the value at the top of the stack,
        !! and k is the value just below the top.
        !!
        !! This function pops both the key and the value from the stack.
        !! As in Lua, this function may trigger a metamethod for the
        !! "newindex" event
        !!
        !! C signature: `void lua_settable (lua_State *L, int index)`
        subroutine lua_settable(l, idx) bind(c, name='lua_settable')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int), intent(in), value    :: idx
        end subroutine lua_settable

        !> @brief Returns the status of the thread `L`.
        !!
        !! The status can be 0 (`LUA_TH_OK`) for a normal thread, an error
        !! code if the thread finished the execution of a `lua_resume`
        !! with an error, or `LUA_TH_YIELD` if the thread is suspended.
        !!
        !! You can only call functions in threads with status `LUA_TH_OK`.
        !! You can resume threads with status `LUA_TH_OK` (to start a new
        !! coroutine) or `LUA_TH_YIELD` (to resume a coroutine).
        !!
        !! C signature: `int lua_status(lua_State *L)`
        function lua_status(l) bind(c, name='lua_status')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
            ! Return value
            integer(kind=c_int)            :: lua_status
        end function lua_status

        !> @brief Converts the zero-terminated string s to a number,
        !! pushes that number into the stack, and returns the total size
        !! of the string, that is, its length plus one.
        !!
        !! The conversion can result in an integer or a float, according
        !! to the lexical conventions of Lua. The string may have
        !! leading and trailing spaces and a sign. If the string is not
        !! a valid numeral, returns 0 and pushes nothing. (Note that the
        !! result can be used as a boolean, `true` if the conversion
        !! succeeds.)
        !!
        !! @note This C function wrapper returns a null-terminated
        !! C string, not a Fortran-style string. Use `lua_stringtonumber`
        !! instead.
        !!
        !! C signature: `size_t lua_stringtonumber (lua_State *L, const char *s)`
        function lua_stringtonumber_(l, s) bind(c, name='lua_stringtonumber')
            import :: c_char, c_size_t, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> String containing a Lua chunk
            character(kind=c_char), intent(in)        :: s
            ! Return value
            integer(kind=c_size_t)                    :: lua_stringtonumber_
        end function lua_stringtonumber_

        !> @brief Converts the Lua value at the given index to
        !! a C boolean value (0 or 1; zero is false, non-zero is true).
        !!
        !! Like all tests in Lua, `lua_toboolean_` returns `true` for any
        !! Lua value different from `false` and `nil`; otherwise it
        !! returns false. (If you want to accept only actual boolean
        !! values, use `lua_isboolean` to test the value's type.)
        !!
        !! @note This C function wrapper returns a Fortran integer, not
        !! a logical value. Use `lua_toboolean` instead.
        !!
        !! C signature: `int lua_toboolean (lua_State *L, int index)`
        function lua_toboolean_(l, idx) bind(c, name='lua_toboolean')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of entry to convert
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)                    :: lua_toboolean_
        end function lua_toboolean_

        !> @brief Converts a value at the given index to a C function.
        !! That value must be a C function; otherwise, returns `NULL`.
        !!
        !! C signature: `lua_CFunction lua_tocfunction (lua_State *L, int index)`
        function lua_tocfunction(l, idx) bind(c, name='lua_tocfunction')
            import :: c_int, c_ptr, c_funptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of entry to convert
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            type(c_funptr)                         :: lua_tocfunction
        end function lua_tocfunction

        !> @brief Converts the Lua value at the given index to the
        !! signed integral type `lua_Integer`.
        !!
        !! The Lua value must be an integer, or a number or string
        !! convertible to an integer; otherwise, `lua_tointegerx`
        !! returns 0.
        !!
        !! If `isnum` is not `NULL`, its referent is assigned a
        !! boolean value that indicates whether the operation succeeded.
        !!
        !! C signature: `lua_Integer lua_tointegerx(lua_State *L, int idx, int *isnum)`
        function lua_tointegerx(l, idx, isnum) bind(c, name='lua_tointegerx')
            import :: c_int, c_ptr, c_lua_integer
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of entry to convert
            integer(kind=c_int), intent(in), value :: idx
            !> Operation success flag
            type(c_ptr),         intent(in), value :: isnum
            ! Return value
            integer(kind=c_lua_integer)              :: lua_tointegerx
        end function lua_tointegerx

        !> @brief Converts the Lua value at the given index to the C type `lua_Number`.
        !!
        !! The Lua value must be a number or a string convertible to a
        !! number; otherwise, `lua_tonumberx` returns 0.
        !!
        !! If `isnum` is not `NULL`, its referent is assigned a boolean
        !! value that indicates whether the operation succeeded.
        !!
        !! C signature: `lua_Number lua_tonumberx (lua_State *L, int index, int *isnum);`
        function lua_tonumberx(l, idx, isnum) bind(c, name='lua_tonumberx')
            import :: c_int, c_ptr, c_lua_number
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of entry to convert
            integer(kind=c_int), intent(in), value :: idx
            !> Operation success flag
            type(c_ptr),         intent(in), value :: isnum
            ! Return value
            real(kind=c_lua_number)                    :: lua_tonumberx
        end function lua_tonumberx

        !> @brief Converts the value at the given index to a generic C
        !! pointer (`void*`).
        !!
        !! The value can be a userdata, a table, a thread, or a
        !! function; otherwise, `lua_topointer` returns `NULL`.
        !! Different objects will give different pointers. There is no
        !! way to convert the pointer back to its original value.
        !!
        !! Typically this function is used only for hashing and debug
        !! information.
        !!
        !! C signature: `const void *lua_topointer (lua_State *L, int index)`
        function lua_topointer(l, idx) bind(c, name='lua_topointer')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of entry to convert
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            type(c_ptr)                            :: lua_topointer
        end function lua_topointer

        !> @brief Converts the value at the given index to a Lua thread
        !! (represented as `lua_State*`).
        !!
        !! This value must be a thread; otherwise, the function returns
        !! `NULL`.
        !!
        !! C signature: `lua_State *lua_tothread (lua_State *L, int index)`
        function lua_tothread(l, idx) bind(c, name='lua_tothread')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of thread
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            type(c_ptr)                            :: lua_tothread
        end function lua_tothread

        !> @brief If the value at the given index is a full userdata,
        !! returns its block address. If the value is a light userdata,
        !! returns its pointer. Otherwise, returns `NULL`.
        !!
        !! C signature: `void *lua_touserdata (lua_State *L, int index)`
        function lua_touserdata(l, idx) bind(c, name='lua_touserdata')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to test
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            type(c_ptr)                            :: lua_touserdata
        end function lua_touserdata

        !> @brief Converts the Lua value at the given index to a C
        !! string.
        !!
        !! If `len` is not `NULL`, it sets `*len` with the string
        !! length. The Lua value must be a string or a number;
        !! otherwise, the function returns `NULL`. If the value is a
        !! number, then `lua_tolstring` also changes the actual value in
        !! the stack to a string. (This change confuses `lua_next` when
        !! `lua_tolstring` is applied to keys during a table traversal.)
        !!
        !! `lua_tolstring` returns a pointer to a string inside the Lua
        !! state. This string always has a zero (`'\0'`) after its last
        !! character (as in C), but can contain other zeros in its body.
        !!
        !! Because Lua has garbage collection, there is no guarantee
        !! that the pointer returned by `lua_tolstring` will be valid
        !! after the corresponding Lua value is removed from the stack.
        !!
        !! C signature: `const char *lua_tolstring(lua_State *L, int idx, size_t *len)`
        function lua_tolstring(l, idx, len) bind(c, name='lua_tolstring')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to convert
            integer(kind=c_int), intent(in), value :: idx
            !> String length
            type(c_ptr),         intent(in), value :: len
            ! Return value
            type(c_ptr)                            :: lua_tolstring
        end function lua_tolstring

        !> @brief Returns the type of the value in the given valid
        !! index, or `LUA_TNONE` for a non-valid (but acceptable) index.
        !!
        !! The types returned by `lua_type` are coded by the following
        !! constants defined in `lua.h`:
        !!   * `LUA_TNIL` (0),
        !!   * `LUA_TNUMBER`,
        !!   * `LUA_TBOOLEAN`,
        !!   * `LUA_TSTRING`,
        !!   * `LUA_TTABLE`,
        !!   * `LUA_TFUNCTION`,
        !!   * `LUA_TUSERDATA`,
        !!   * `LUA_TTHREAD`, and
        !!   * `LUA_TLIGHTUSERDATA`
        !!
        !! C signature: `int lua_type(lua_State *L, int idx)`
        function lua_type(l, idx) bind(c, name='lua_type')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of entry in stack
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)                    :: lua_type
        end function lua_type

        !> @brief Returns the name of the type encoded by the value
        !! `tp`, which must be one the values returned by `lua_type`.
        !!
        !! C signature: `const char *lua_typename(lua_State *L, int tp)`
        function lua_typename_(l, tp) bind(c, name='lua_typename')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Lua value type code
            integer(kind=c_int), intent(in), value :: tp
            ! Return value
            type(c_ptr)                            :: lua_typename_
        end function lua_typename_

        !***** requires `LUA_REGISTRYINDEX` from `lua.h`; not exported

        ! !> @brief Returns the pseudo-index that represents the `i`-th
        ! !! upvalue of the running function.
        ! !!
        ! !! `i` can range from 1 to 256. Upvalues refer to variables
        ! !! associated with C closures.
        ! !!
        ! !! C signature: `int lua_upvalueindex (int i)`
        ! function lua_upvalueindex(i) bind(c, name='lua_upvalueindex')
        !     import :: c_int
        !     !> Upvalue index
        !     integer(kind=c_int), intent(in), value :: i
        !     ! Return value
        !     integer(kind=c_int)                    :: lua_upvalueindex
        ! end function lua_upvalueindex

        ! Auxilliary library functions

        !> @brief Loads a file as a Lua chunk.
        !!
        !! This function uses `lua_load` to load the chunk in the file
        !! named `filename`. If `filename` is `NULL`, then it loads from
        !! the standard input. The first line in the file is ignored if
        !! it starts with a `#`.
        !!
        !! The string mode works as in function `lua_load`.
        !!
        !! This function returns the same results as `lua_load`, but it
        !! has an extra error code `LUA_ERRFILE` for file-related errors
        !! (e.g., it cannot open or read the file). That is,
        !!   * `LUA_TH_OK`: no errors;
        !!   * `LUA_TH_ERRSYNTAX`: syntax error during precompilation;
        !!   * `LUA_TH_ERRMEM`: memory allocation (out-of-memory) error;
        !!   * `LUA_TH_ERRGCMM`: error while running a `__gc` metamethod.
        !!     (This error has no relation with the chunk being loaded.
        !!     It is generated by the garbage collector.)
        !!   * `LUA_ERRFILE`: file-related errors
        !!
        !! As `lua_load`, this function only loads the chunk; it does
        !! not run it.
        !!
        !! C signature: `int luaL_loadfilex(lua_State *L, const char *filename, const char *mode)`
        function lual_loadfilex(l, filename, mode) bind(c, name='luaL_loadfilex')
            import :: c_char, c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Name of file containing a Lua chunk
            character(kind=c_char), intent(in)        :: filename
            !> Binary, text, or mixed mode of file (`b`, `t`, or `bt`)
            type(c_ptr),            intent(in), value :: mode
            ! Return value
            integer(kind=c_int)                       :: lual_loadfilex
        end function lual_loadfilex

        !> @brief Loads a string as a Lua chunk.
        !!
        !! This function uses `lua_load` to load the chunk in the
        !! zero-terminated string `s`.
        !!
        !! This function returns the same results as `lua_load`. That is,
        !!   * `LUA_TH_OK`: no errors;
        !!   * `LUA_TH_ERRSYNTAX`: syntax error during precompilation;
        !!   * `LUA_TH_ERRMEM`: memory allocation (out-of-memory) error;
        !!   * `LUA_TH_ERRGCMM`: error while running a `__gc` metamethod.
        !!     (This error has no relation with the chunk being loaded.
        !!     It is generated by the garbage collector.)
        !!
        !! C signature: `int luaL_loadstring (lua_State *L, const char *s)`
        function lual_loadstring_(l, s) bind(c, name='luaL_loadstring')
            import :: c_char, c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> String containing a Lua chunk
            character(kind=c_char), intent(in)        :: s
            ! Return value
            integer(kind=c_int)                       :: lual_loadstring_
        end function lual_loadstring_

        !> @brief If the registry already has the key `tname`,
        !! returns 0. Otherwise, creates a new table to be used as a
        !! metatable for userdata, adds to this new table the pair
        !! `__name = tname`, adds to the registry the pair
        !! `[tname] = new table`, and returns 1.
        !!
        !! The entry `__name` is used by some error-reporting functions.
        !!
        !! In both cases pushes onto the stack the final value
        !! associated with `tname` in the registry.
        !!
        !! C signature: `int luaL_newmetatable (lua_State *L, const char *tname)`
        function lual_newmetatable(l, tname) bind(c, name='lua_newmetatable')
            import :: c_char, c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Registry key name
            character(kind=c_char), intent(in)        :: tname
            ! Return value
            integer(kind=c_int)                       :: lual_newmetatable
        end function lual_newmetatable

        !>  @brief Creates a new Lua state.
        !!
        !! It calls `lua_newstate` with an allocator based on the
        !! standard C `realloc` function and then sets a panic function
        !! that prints an error message to the standard error output in
        !! case of fatal errors.
        !!
        !! Returns the new state, or `NULL` if there is a memory
        !! allocation error.
        !!
        !! C signature: `lua_State *luaL_newstate(void)`
        function lual_newstate() bind(c, name='luaL_newstate')
            import :: c_ptr
            ! Return value, pointer to Lua interpreter state
            type(c_ptr) :: lual_newstate
        end function lual_newstate

        !> @brief Creates and returns a *reference*, in the table at
        !! index `t`, for the object at the top of the stack (and
        !! pops the object).
        !!
        !! A reference is a unique integer key. As long as you do not
        !! manually add integer keys into table `t`, `luaL_ref` ensures
        !! the uniqueness of the key it returns. You can retrieve an
        !! object referred by reference `r` by calling
        !! `lua_rawgeti(L, t, r)`. Function `luaL_unref` frees a
        !! reference and its associated object.
        !!
        !! If the object at the top of the stack is `nil`, `luaL_ref`
        !! returns the constant `LUA_REFNIL`. The constant `LUA_NOREF`
        !! is guaranteed to be different from any reference returned
        !! by `luaL_ref`.
        !!
        !! C signature: `int luaL_ref (lua_State *L, int t)`
        function lual_ref(l, t) bind(c, name='luaL_ref')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int),    intent(in), value :: t

            ! Return value
            integer(kind=c_int)                       :: lual_ref
        end function lual_ref

        !> @brief Releases reference `ref` from the table at index `t`
        !! (see `luaL_ref`).
        !!
        !! The entry is removed from the table, so that the referred
        !! object can be collected. The reference `ref` is also freed to
        !! be used again.
        !!
        !! If `ref` is `LUA_NOREF` or `LUA_REFNIL`, `luaL_unref` does
        !! nothing.
        !!
        !! C signature: `void luaL_unref (lua_State *L, int t, int ref)`
        subroutine lual_unref(l, t, ref) bind(c, name='luaL_unref')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int),    intent(in), value :: t
            !> Reference ID
            integer(kind=c_int),    intent(in), value :: ref
        end subroutine lual_unref

        !> @brief Pushes onto the stack a string identifying the current
        !! position of the control at level `lvl` in the call stack.
        !!
        !! Typically this string has the following format:
        !!
        !!      chunkname:currentline:
        !!
        !! Level 0 is the running function, level 1 is the function that
        !! called the running function, etc.
        !!
        !! This function is used to build a prefix for error messages.
        !!
        !! C signature: `void luaL_where (lua_State *L, int lvl)`
        subroutine lual_where(l, lvl) bind(c, name='luaL_where')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int),    intent(in), value :: lvl
        end subroutine lual_where

        ! Back to Lua C API functions

        !> @brief Calls a function in protected mode with the ability to
        !! yield
        !!
        !! Both `nargs` and `nresults` have the same meaning as in
        !! `lua_call`. If there are no errors during the call,
        !! `lua_pcallk` behaves exactly like `lua_callk`. However, if
        !! there is any error, `lua_pcallk` catches it, pushes a single
        !! value on the stack (the error object), and returns an error
        !! code. Like `lua_call`, `lua_pcallk` always removes the
        !! function and its arguments from the stack.
        !!
        !! If `msgh` is 0, then the error object returned on the stack
        !! is exactly the original error object. Otherwise, `msgh` is
        !! the stack index of a message handler. (This index cannot be a
        !! pseudo-index.) In case of runtime errors, this function will
        !! be called with the error object and its return value will be
        !! the object returned on the stack by `lua_pcallk`.
        !!
        !! Typically, the message handler is used to add more debug
        !! information to the error object, such as a stack traceback.
        !! Such information cannot be gathered after the return of
        !! `lua_pcallk`, since by then the stack has unwound.
        !!
        !! The `lua_pcallk` function returns one of the following
        !! constants (defined in `lua.h`):
        !!   * `LUA_TH_OK` (0): success.
        !!   * `LUA_TH_ERRRUN`: a runtime error.
        !!   * `LUA_TH_ERRMEM`: memory allocation error. For such errors,
        !!     Lua does not call the message handler.
        !!   * `LUA_TH_ERRERR`: error while running the message handler.
        !!   * `LUA_TH_ERRGCMM`: error while running a `__gc` metamethod.
        !!     For such errors, Lua does not call the message handler
        !!     (as this kind of error typically has no relation with the
        !!     function being called).
        !!
        !! C signature: `int lua_pcallk(lua_State *L, int nargs, int nresults, int msgh, lua_KContext ctx, lua_KFunction k)`
        function lua_pcallk(l, nargs, nresults, msgh, ctx, k) bind(c, name='lua_pcallk')
            import :: c_funptr, c_int, c_intptr_t, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),              intent(in), value :: l
            !> The number of arguments pushed onto the stack
            integer(kind=c_int),      intent(in), value :: nargs
            !> The maximum number of results to return unless
            !! `nresults` is `LUA_TH_MULTRET`
            integer(kind=c_int),      intent(in), value :: nresults
            !> Stack index of the message handler
            integer(kind=c_int),      intent(in), value :: msgh
            !> The continuation-function context (type is c_intptr_t)
            integer(kind=c_int),      intent(in)        :: ctx
            ! integer(kind=c_intptr_t), intent(in), value :: ctx
            !> Pointer to continuation-function, for handling yield cases
            type(c_funptr),           intent(in), value :: k
            ! Return value
            integer(kind=c_int)                         :: lua_pcallk
        end function lua_pcallk

        !> @brief Pushes the string pointed to by `s` with size `len`
        !! onto the stack.
        !!
        !! Lua makes (or reuses) an internal copy of the given string,
        !! so the memory at `s` can be freed or reused immediately
        !! after the function returns. The string can contain any
        !! binary data, including embedded zeros.
        !!
        !! Returns a pointer to the internal copy of the string.
        !!
        !! C signature: `const char *lua_pushlstring(lua_State *L, const char *s, size_t len)`
        function lua_pushlstring(l, s, len) bind(c, name='lua_pushlstring')
            import :: c_char, c_ptr, c_size_t
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> String to push onto stack
            character(kind=c_char), intent(in)        :: s
            !> String length
            integer(kind=c_size_t), intent(in), value :: len
            ! Return value
            type(c_ptr)                               :: lua_pushlstring
        end function lua_pushlstring

        !> @brief Pushes the zero-terminated string pointed to by `s`
        !! onto the stack.
        !!
        !! Lua makes (or reuses) an internal copy of the given string,
        !! so the memory at `s` can be freed or reused immediately
        !! after the function returns.
        !!
        !! Returns a pointer to the internal copy of the string.
        !!
        !! If `s` is `NULL`, pushes `nil` and returns `NULL`.
        !!
        !! C signature: `const char *lua_pushstring(lua_State *L, const char *s)`
        function lua_pushstring(l, s) bind(c, name='lua_pushstring')
            import :: c_char, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> String to push onto stack
            character(kind=c_char), intent(in)        :: s
            ! Return value
            type(c_ptr)                               :: lua_pushstring
        end function lua_pushstring

        !> @brief Pushes the thread represented by `L` onto the stack.
        !!
        !! Returns 1 if this thread is the main thread of its state.
        !!
        !! C signature: `int lua_pushthread(lua_State *L)`
        function lua_pushthread(l) bind(c, name='lua_pushthread')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
            ! Return value
            integer(kind=c_int)            :: lua_pushthread
        end function lua_pushthread

        !> @brief Performs an arithmetic or bitwise operation over the
        !! two values (or one, in the case of negations) at the top of
        !! the stack, with the value at the top being the second
        !! operand, pops these values, and pushes the result of the
        !! operation.
        !!
        !! The function follows the semantics of the corresponding Lua
        !! operator (that is, it may call metamethods).
        !!
        !! The value of `op` must be one of the following constants:
        !!   * `LUA_OPADD`: performs addition (`+`)
        !!   * `LUA_OPSUB`: performs subtraction (`-`)
        !!   * `LUA_OPMUL`: performs multiplication (`*`)
        !!   * `LUA_OPDIV`: performs float division (`/`)
        !!   * `LUA_OPIDIV`: performs floor division (`//`)
        !!   * `LUA_OPMOD`: performs modulo (`%`)
        !!   * `LUA_OPPOW`: performs exponentiation (`^`)
        !!   * `LUA_OPUNM`: performs mathematical negation (unary `-`)
        !!   * `LUA_OPBNOT`: performs bitwise NOT (`~`)
        !!   * `LUA_OPBAND`: performs bitwise AND (`&`)
        !!   * `LUA_OPBOR`: performs bitwise OR (`|`)
        !!   * `LUA_OPBXOR`: performs bitwise exclusive OR (`~`)
        !!   * `LUA_OPSHL`: performs left shift (`<<`)
        !!   * `LUA_OPSHR`: performs right shift (`>>`)
        !!
        !! C signature: `void lua_arith(lua_State *L, int op)`
        subroutine lua_arith(l, op) bind(c, name='lua_arith')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Arithmetic or bitwise operator code
            integer(kind=c_int), intent(in), value :: op
        end subroutine lua_arith

        !> @brief Calls a function which may yield
        !!
        !! To call a function you must use the following protocol:
        !!   1) first, the function to be called is pushed onto the
        !!      stack;
        !!   2) then, the arguments to the function are pushed in
        !!      direct order; that is, the first argument is pushed
        !!      first.
        !!   3) Finally you call `lua_callk`;
        !!
        !! `nargs` is the number of arguments that you pushed onto the
        !! stack. All arguments and the function value are popped from
        !! the stack when the function is called. The function results
        !! are pushed onto the stack when the function returns. The
        !! number of results is adjusted to `nresults`, unless
        !! `nresults` is `LUA_TH_MULTRET`. In this case, all results from
        !! the function are pushed; Lua takes care that the returned
        !! values fit into the stack space, but it does not ensure any
        !! extra space in the stack. The function results are pushed
        !! onto the stack in direct order (the first result is pushed
        !! first), so that after the call the last result is on the
        !! top of the stack.
        !!
        !! Any error inside the called function is propagated upwards
        !! (with a `longjmp`).
        !!
        !! C signature: `void lua_callk(lua_State *L, int nargs, int nresults, lua_KContext ctx, lua_CFunction k)`
        subroutine lua_callk(l, nargs, nresults, ctx, k) bind(c, name='lua_callk')
            import :: c_funptr, c_int, c_intptr_t, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),              intent(in), value :: l
            !> Number of arguments pushed onto stack
            integer(kind=c_int),      intent(in), value :: nargs
            !> Maximum number of results
            integer(kind=c_int),      intent(in), value :: nresults
            !> The continuation-function context (type is c_intptr_t)
            integer(kind=c_int),      intent(in)        :: ctx
            ! integer(kind=c_intptr_t), intent(in), value :: ctx
            !> Pointer to the continuation-function, for handling yield
            !! cases
            type(c_funptr),           intent(in), value :: k
        end subroutine lua_callk

        !> @brief Destroys all objects in the given Lua state (calling
        !! the corresponding garbage-collection metamethods, if any) and
        !! frees all dynamic memory used by this state.
        !!
        !! In several platforms, you may not need to call this function,
        !! because all resources are naturally released when the host
        !! program ends. On the other hand, long-running programs that
        !! create multiple states, such as daemons or web servers, will
        !! probably need to close states as soon as they are not needed.
        !!
        !! C signature: `void lua_close(lua_State *L)`
        subroutine lua_close(l) bind(c, name='lua_close')
            import :: c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
        end subroutine lua_close

        !> @brief Concatenates the `n` values at the top of the stack,
        !! pops them, and leaves the result at the top.
        !!
        !! If `n` is 1, the result is the single value on the stack
        !! (that is, the function does nothing); if `n` is 0, the result
        !! is the empty string. Concatenation is performed following the
        !! usual semantics of Lua
        !!
        !! C signature: `void lua_concat(lua_State *L, int n)`
        subroutine lua_concat(l, n) bind(c, name='lua_concat')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Number of stack values to concatenate
            integer(kind=c_int), intent(in), value :: n
        end subroutine lua_concat

        !> @brief Copies the element at index `fromidx` into the valid
        !! index `toidx`, replacing the value at that position.
        !!
        !! Values at other positions are not affected.
        !!
        !! C signature: `void lua_copy(lua_State *L, int fromidx, int toidx)`
        subroutine lua_copy(l, fromidx, toidx) bind(c, name='lua_copy')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Source element index
            integer(kind=c_int), intent(in), value :: fromidx
            !> Destination element index
            integer(kind=c_int), intent(in), value :: toidx
        end subroutine lua_copy

        !> @brief Creates a new empty table and pushes it onto the
        !! stack, with hints to preallocate size.
        !!
        !! Parameter `narr` is a hint for how many elements the table
        !! will have as a sequence; parameter `nrec` is a hint for how
        !! many other elements the table will have. Lua may use these
        !! hints to preallocate memory for the new table. This
        !! preallocation is useful for performance when you know in
        !! advance how many elements the table will have. Otherwise you
        !! can use the function `lua_newtable`.
        !!
        !! C signature: `void lua_createtable(lua_State *L, int narr, int nrec)`
        subroutine lua_createtable(l, narr, nrec) bind(c, name='lua_createtable')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Hint of number of elements in the table as a sequence
            !! (with implied numeric index; array-like)
            integer(kind=c_int), intent(in), value :: narr
            !> Hint of number of other elements in the table
            !! (with explicit key; dict-like)
            integer(kind=c_int), intent(in), value :: nrec
        end subroutine lua_createtable

        !> @brief Pushes a boolean value with value `b` onto the stack.
        !!
        !! C signature: `void lua_pushboolean(lua_State *L, int b)`
        subroutine lua_pushboolean(l, b) bind(c, name='lua_pushboolean')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Boolean value to push onto the stack
            integer(kind=c_int), intent(in), value :: b
        end subroutine lua_pushboolean

        !> @brief Pushes a new C closure onto the stack.
        !!
        !! When a C function is created, it is possible to associate
        !! some values with it, thus creating a C closure; these values
        !! are then accessible to the function whenever it is called.
        !! To associate values with a C function, first these values
        !! must be pushed onto the stack (when there are multiple
        !! values, the first value is pushed first). Then
        !! `lua_pushcclosure` is called to create and push the C
        !! function onto the stack, with the argument `n` telling how
        !! many values will be associated with the function.
        !! `lua_pushcclosure` also pops these values from the stack.
        !!
        !! C signature: `void lua_pushcclosure(lua_State *L, lua_CFunction fn, int n)`
        subroutine lua_pushcclosure(l, fn, n) bind(c, name='lua_pushcclosure')
            import :: c_funptr, c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Pointer to C closure function
            type(c_funptr),      intent(in), value :: fn
            !> Number of values associated with the closure
            integer(kind=c_int), intent(in), value :: n
        end subroutine lua_pushcclosure

        !> @brief Pushes an integer with value `n` onto the stack.
        !!
        !! C signature: `void lua_pushinteger(lua_State *L, lua_Integer n)`
        subroutine lua_pushinteger(l, n) bind(c, name='lua_pushinteger')
            import :: c_lua_integer, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Integer value to push onto the stack
            integer(kind=c_lua_integer), intent(in), value :: n
        end subroutine lua_pushinteger

        !> @brief Pushes a light userdata onto the stack.
        !!
        !! Userdata represent C values in Lua. A light userdata
        !! represents a pointer, a `void*`. It is a value (like a
        !! number): you do not create it, it has no individual
        !! metatable, and it is not collected (as it was never created).
        !! A light userdata is equal to "any" light userdata with the
        !! same C address.
        !!
        !! C signature: `void  lua_pushlightuserdata(lua_State *L, void *p)`
        subroutine lua_pushlightuserdata(l, p) bind(c, name='lua_pushlightuserdata')
            import :: c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
            !> Pointer representing a light userdata
            type(c_ptr), intent(in), value :: p
        end subroutine lua_pushlightuserdata

        !> @brief Pushes a `nil` value onto the stack.
        !!
        !! C signature: `void lua_pushnil(lua_State *L)`
        subroutine lua_pushnil(l) bind(c, name='lua_pushnil')
            import :: c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
        end subroutine lua_pushnil

        !> @brief Pushes a `lua_Number` (`c_double`) with value `n` onto
        !! the stack.
        !!
        !! C signature: `void lua_pushnumber(lua_State *L, lua_Number n)`
        subroutine lua_pushnumber(l, n) bind(c, name='lua_pushnumber')
            import :: c_lua_number, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),        intent(in), value :: l
            !> Float to push onto the stack
            real(kind=c_lua_number), intent(in), value :: n
        end subroutine lua_pushnumber

        !> @brief Pushes a copy of the element at the given index onto the stack.
        !!
        !! C signature: `void  lua_pushvalue(lua_State *L, int idx)`
        subroutine lua_pushvalue(l, idx) bind(c, name='lua_pushvalue')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to copy and push onto stack
            integer(kind=c_int), intent(in), value :: idx
        end subroutine lua_pushvalue

        !> @brief Returns 1 (true) if the two values in indices index1
        !! and index2 are primitively equal (that is, without calling
        !! the `__eq` metamethod).
        !!
        !! Otherwise returns 0 (false). Also returns 0 if any of the
        !! indices are not valid.
        !!
        !! C signature: `int lua_rawequal (lua_State *L, int index1, int index2)`
        function lua_rawequal(l, idx1, idx2) bind(c, name='lua_rawequal')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
            !> Stack index of first value
            integer(kind=c_int), intent(in), value :: idx1
            !> Stack index of second value
            integer(kind=c_int), intent(in), value :: idx2
            ! Return value
            integer(kind=c_int)            :: lua_rawequal
        end function lua_rawequal

        !> @brief Pushes onto the stack the value `t[k]`, where t is the
        !! value at the given index and `k` is the value at the top of
        !! the stack.
        !!
        !! This function pops the key from the stack, pushing the
        !! resulting value in its place. Unlike `lua_gettable`, this
        !! function does not trigger metamethods.
        !!
        !! Returns the type of the pushed value.
        !!
        !! C signature: `int lua_rawget (lua_State *L, int index)`
        function lua_rawget(l, idx) bind(c, name='lua_rawget')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
            !> Stack index of table
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_int)            :: lua_rawget
        end function lua_rawget

        !> @brief Pushes onto the stack the value of the indexed table
        !! element `t[n]`. Returns the type of that value.
        !!
        !! Similar to `lua_geti` but uses "raw" access and does not
        !! trigger metamethods.
        !!
        !! C signature: `int lua_rawgeti (lua_State *L, int index, lua_Integer n)`
        function lua_rawgeti(l, idx, n) bind(c, name='lua_rawgeti')
            import :: c_int, c_ptr, c_lua_integer
            !> Pointer to Lua interpreter state
            type(c_ptr),             intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int), intent(in), value     :: idx
            !> Index of value in table
            integer(kind=c_lua_integer), intent(in), value :: n
            ! Return value
            integer(kind=c_int)                        :: lua_rawgeti
        end function lua_rawgeti

        !> @brief Pushes onto the stack the value t[`k`], where `t` is
        !! the table at the given index and `k` is the pointer `p`
        !! represented as a light userdata.
        !!
        !! The access is raw; that is, it does not invoke the `__index` metamethod.
        !! Returns the type of the pushed value.
        !!
        !! C signature: `int lua_rawgetp (lua_State *L, int index, const void *p)`
        function lua_rawgetp(l, idx, p) bind(c, name='lua_rawgetp')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int), intent(in), value :: idx
            !> Pointer to be used as table key
            type(c_ptr),         intent(in), value :: p
            ! Return value
            integer(kind=c_int)                    :: lua_rawgetp
        end function lua_rawgetp

        !> @brief Returns the raw "length" of the value at the given index.
        !!
        !! For strings, this is the string length; for tables, this is the
        !! result of the length operator ('`#`') with no metamethods; for
        !! userdata, this is the size of the block of memory allocated for
        !! the userdata; for other values, it is 0.
        !!
        !! C signature: `size_t lua_rawlen (lua_State *L, int index)`
        function lua_rawlen(l, idx) bind(c, name='lua_rawlen')
            import :: c_int, c_ptr, c_size_t
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element to test
            integer(kind=c_int), intent(in), value :: idx
            ! Return value
            integer(kind=c_size_t)                 :: lua_rawlen
        end function lua_rawlen

        !> @brief Does the equivalent to `t[k] = v`, where `t` is the
        !! value at the given index, `v` is the value at the top of the
        !! stack, and `k` is the value just below the top.
        !!
        !! This function pops both the key and the value from the stack.
        !! Unlike `lua_settable`, this function will not trigger
        !! metamethods.
        !!
        !! C signature: `void lua_rawset (lua_State *L, int index)`
        subroutine lua_rawset(l, idx) bind(c, name='lua_rawset')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int), intent(in), value    :: idx
        end subroutine lua_rawset

        !> @brief Does the equivalent of `t[i] = v`, where `t` is the
        !! table at the given index and `v` is the value at the top of
        !! the stack.
        !!
        !! This function pops the value from the stack. The assignment
        !! is raw, that is, it does not invoke the `__newindex` metamethod.
        !!
        !! C signature: `void lua_rawseti (lua_State *L, int index, lua_Integer i)`
        subroutine lua_rawseti(l, idx, i) bind(c, name='lua_rawseti')
            import :: c_int, c_ptr, c_lua_integer
            !> Pointer to Lua interpreter state
            type(c_ptr),               intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int),       intent(in), value :: idx
            !> Index of value in table
            integer(kind=c_lua_integer), intent(in), value :: i
        end subroutine lua_rawseti

        !> @brief Does the equivalent of `t[p] = v`, where `t` is the
        !! table at the given index, `p` is encoded as a light userdata,
        !! and `v` is the value at the top of the stack.
        !!
        !! This function pops the value from the stack. The assignment
        !! is raw, that is, it does not invoke `__newindex` metamethod.
        !!
        !! C signature: `void lua_rawsetp (lua_State *L, int index, const void *p)`
        subroutine lua_rawsetp(l, idx, p) bind(c, name='lua_rawsetp')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of table on stack
            integer(kind=c_int), intent(in), value :: idx
            !> Pointer to be used as table key
            type(c_ptr),         intent(in), value :: p
        end subroutine lua_rawsetp

        !> @brief Rotates the stack elements between the valid index
        !! `idx` and the top of the stack.
        !!
        !! The elements are rotated `n` positions in the direction of
        !! the top, for a positive `n`, or `-n` positions in the
        !! direction of the bottom, for a negative `n`. The absolute
        !! value of `n` must not be greater than the size of the slice
        !! being rotated. This function cannot be called with a
        !! pseudo-index, because a pseudo-index is not an actual stack
        !! position.
        !!
        !! C signature: `void lua_rotate (lua_State *L, int idx, int n)`
        subroutine lua_rotate(l, idx, n) bind(c, name='lua_rotate')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of element acting as endpoint of rotation slice
            integer(kind=c_int), intent(in), value :: idx
            !> Positions rorated in the direction of the top of the
            !! stack
            integer(kind=c_int), intent(in), value :: n
        end subroutine lua_rotate

        !> @brief Pops a value from the stack and sets it as the new
        !! value of global `name`.
        !!
        !! C signature: `void lua_setglobal(lua_State *L, const char *name)`
        subroutine lua_setglobal(l, name) bind(c, name='lua_setglobal')
            import :: c_char, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> New name of popped element
            character(kind=c_char), intent(in)        :: name
        end subroutine lua_setglobal

        !> @brief Accepts any index, or 0, and sets the stack top to
        !! this index.
        !!
        !! If the new top is larger than the old one, then the new
        !! elements are filled with `nil`. If index is 0, then all stack
        !! elements are removed.
        !!
        !! C signature: `void lua_settop(lua_State *L, int idx)`
        subroutine lua_settop(l, idx) bind(c, name='lua_settop')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index to set as top of the stack
            integer(kind=c_int), intent(in), value :: idx
        end subroutine lua_settop

        !> @brief Pops a value from the stack and sets it as the new
        !! value associated to the full userdata at the given index.
        !!
        !! C signature: `void lua_setuservalue (lua_State *L, int index)`
        subroutine lua_setuservalue(l, idx) bind(c, name='lua_setuservalue')
            import :: c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index to set as top of the stack
            integer(kind=c_int), intent(in), value :: idx
        end subroutine lua_setuservalue

        !> @brief Returns the address of the version number (a C static
        !! variable) stored in the Lua core.
        !!
        !! When called with a valid `lua_State`, returns the address of
        !! the version used to create that state. When called with
        !! `NULL`, returns the address of the version running the call.
        !!
        !! C signature: `const lua_Number *lua_version (lua_State *L)`
        function lua_version_(l) bind(c, name='lua_version')
            import :: c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            ! Return value
            type(c_ptr)                            :: lua_version_
        end function lua_version_

        !> @brief Exchange values between different threads of the same
        !! state.
        !!
        !! This function pops `n` values from the stack `from`, and
        !! pushes them onto the stack `to`.
        !!
        !! C signature: `void lua_xmove (lua_State *from, lua_State *to, int n)`
        subroutine lua_xmove(from, to, n) bind(c, name='lua_xmove')
            import :: c_int, c_ptr
            !> Pointer to source stack
            type(c_ptr),            intent(in), value :: from
            !> Pointer to destination stack
            type(c_ptr),            intent(in), value :: to
            !> Number of arguments to move
            integer(kind=c_int),    intent(in), value :: n
        end subroutine lua_xmove

        !> @brief  Yields a coroutine (thread).
        !!
        !! When a C function calls `lua_yieldk`, the running coroutine
        !! suspends its execution, and the call to `lua_resume` that
        !! started this coroutine returns. The parameter `nresults` is
        !! the number of values from the stack that will be passed as
        !! results to `lua_resume`.
        !!
        !! When the coroutine is resumed again, Lua calls the given
        !! continuation function `k` to continue the execution of the
        !! C function that yielded. This continuation function receives
        !! the same stack from the previous function, with the `n`
        !! results removed and replaced by the arguments passed to
        !! `lua_resume`. Moreover, the continuation function receives
        !! the value `ctx` that was passed to `lua_yieldk`.
        !!
        !! Usually, this function does not return; when the coroutine
        !! eventually resumes, it continues executing the continuation
        !! function. However, there is one special case, which is when
        !! this function is called from inside a line or a count hook.
        !! In that case, `lua_yieldk` should be called with no
        !! continuation (probably in the form of `lua_yield`) and no
        !! results, and the hook should return immediately after the
        !! call. Lua will yield and, when the coroutine resumes again,
        !! it will continue the normal execution of the (Lua) function
        !! that triggered the hook.
        !!
        !! This function can raise an error if it is called from a
        !! thread with a pending C call with no continuation function,
        !! or it is called from a thread that is not running inside a
        !! resume (e.g., the main thread).
        !!
        !! C signature: `int lua_yieldk (lua_State *L, int nresults, lua_KContext ctx, lua_KFunction k);`
        function lua_yieldk(l, nresults, ctx, k) bind(c, name='lua_yieldk')
            import :: c_funptr, c_int, c_intptr_t, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),              intent(in), value :: l
            !> Maximum number of results
            integer(kind=c_int),      intent(in), value :: nresults
            !> The continuation-function context
            integer(kind=c_int),      intent(in) :: ctx
            ! integer(kind=c_intptr_t), intent(in), value :: ctx
            !> Pointer to the continuation-function
            type(c_funptr),           intent(in), value :: k
            ! Return value
            integer(kind=c_int)                         :: lua_yieldk
        end function lua_yieldk

        ! Auxilliary library function

        !***** Not used; the C version of `luaL_argcheck` is implemented
        !***** as a macro so this interface cannot be bound to the C API

        ! !> @brief Checks whether `cond` is true. If it is not, raises an
        ! !! error reporting a problem with argument `arg` of the
        ! !! C function that called it, using a standard message that
        ! !! includes extramsg as a comment:
        ! !!
        ! !!      bad argument #arg to 'funcname' (extramsg)
        ! !!
        ! !! C signature: `void luaL_argcheck (lua_State *L, int cond, int arg, const char *extramsg)`
        ! subroutine lual_argcheck_(l, cond, arg, extramsg) bind(c, name='lual_argcheck')
        !     import :: c_ptr, c_int, c_char
        !     !> Pointer to Lua interpreter state
        !     type(c_ptr),            intent(in), value :: l
        !     !> Logical 0/1 indicating true/false
        !     integer(kind=c_int),    intent(in), value :: cond
        !     !> Position of problematic argument in function argument list
        !     integer(kind=c_int),    intent(in), value :: arg
        !     !> C format (null terminated) string containing additional error detail
        !     character(kind=c_char), intent(in)        :: extramsg
        ! end subroutine lual_argcheck_

        !***** Linking error on Windows & Linux - extern "C" problem?
        ! !> @brief Raises an error reporting a problem with argument
        ! !! `arg` of the C function that called it, using a standard
        ! !! message that includes `extramsg` as a comment:
        ! !!
        ! !!      bad argument #arg to 'funcname' (extramsg)
        ! !!
        ! !! This function never returns.
        ! !!
        ! !! C signature: `int luaL_argerror (lua_State *L, int arg, const char *extramsg)`
        ! function lual_argerror_(l, arg, extramsg) bind(c, name='lual_argerror')
        !     import :: c_ptr, c_int, c_char
        !     !> Pointer to Lua interpreter state
        !     type(c_ptr),            intent(in), value :: l
        !     !> Position of problematic argument in function argument list
        !     integer(kind=c_int),    intent(in), value :: arg
        !     !> C format (null terminated) string containing additional error detail
        !     character(kind=c_char), intent(in)        :: extramsg

        !     ! Return value (needed for implementation of luaL_argcheck macro)
        !     integer(kind=c_int)                       :: lual_argerror_
        ! end function lual_argerror_

        !> @brief Calls a metamethod.
        !!
        !! If the object at index `obj` has a metatable and this metatable has
        !! a field `e`, this function calls this field passing the object as
        !! its only argument. In this case this function returns `true` and
        !! pushes onto the stack the value returned by the call. If there is
        !! no metatable or no metamethod, this function returns `false`
        !! (without pushing any value on the stack).
        !!
        !! C signature: `int luaL_callmeta (lua_State *L, int obj, const char *e)`
        function lual_callmeta(l, obj, e) bind(c, name='luaL_callmeta')
            import :: c_ptr, c_int, c_char
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of object on stack
            integer(kind=c_int),    intent(in), value :: obj
            !> Metamethod name
            character(kind=c_char), intent(in)        :: e

            ! Return value
            integer(kind=c_int)                       :: lual_callmeta
        end function lual_callmeta

        !> @brief Checks whether the function has an argument of any
        !! type (including `nil`) at position `arg`.
        !!
        !! C signature: `void luaL_checkany (lua_State *L, int arg)`
        subroutine lual_checkany(l, arg) bind(c, name='luaL_checkany')
            import :: c_ptr, c_int
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
            !> Called function argument position
            integer(kind=c_int),    intent(in), value :: arg
        end subroutine lual_checkany

        !> @brief Checks whether the function has an argument of any
        !! type (including `nil`) at position `arg`.
        !!
        !! C signature: `lua_Integer luaL_checkinteger (lua_State *L, int arg)`
        function lual_checkinteger(l, arg) bind(c, name='luaL_checkinteger')
            import :: c_ptr, c_int, c_lua_integer
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Called function argument position
            integer(kind=c_int), intent(in), value :: arg

            ! Return value
            integer(kind=c_lua_integer)              :: lual_checkinteger
        end function lual_checkinteger

        !> @brief Checks whether the function argument `arg` is a string
        !! and returns this string; if `l` is not `NULL` fills `*l` with
        !! the string's length.
        !!
        !! This function uses `lua_tolstring` to get its result, so all
        !! conversions and caveats of that function apply here.
        !!
        !! @note This is thin wrapper around the C auxiliary function.
        !! Application developers should call the Fortran functions
        !! `lual_checklstring` or `lual_checkstring` instead.
        !!
        !! C signature: `const char *luaL_checklstring (lua_State *L, int arg, size_t *l)`
        function lual_checklstring_(l, arg, len) bind(c, name='luaL_checklstring')
            import :: c_int, c_ptr, c_size_t
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Index of element to convert
            integer(kind=c_int),    intent(in), value :: arg
            !> String length
            integer(kind=c_size_t), intent(inout)     :: len

            ! Return value
            type(c_ptr)                               :: lual_checklstring_
        end function lual_checklstring_

        !> @brief Checks whether the function argument `arg` is a number
        !! and returns this number.
        !!
        !! C signature: `lua_Number luaL_checknumber (lua_State *L, int arg)`
        function lual_checknumber_(l, arg) bind(c, name='luaL_checknumber')
            import :: c_int, c_ptr, c_lua_number
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Index of function argument to check
            integer(kind=c_int), intent(in), value :: arg
            ! Return value
            real(kind=c_lua_number)                    :: lual_checknumber_
        end function lual_checknumber_

        !> @brief Grows the stack size to `top + sz` elements, raising an
        !! error if the stack cannot grow to that size.
        !!
        !! `msg` is an additional text to go into the error message
        !! (or `NULL` for no additional text).
        !!
        !! C signature: `void luaL_checkstack (lua_State *L, int sz, const char *msg)`
        subroutine lual_checkstack(l, sz, msg) bind(c, name='luaL_checkstack')
            import :: c_ptr, c_int, c_char
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Number of elements to add to stack
            integer(kind=c_int),    intent(in), value :: sz
            !> Additional error message
            character(kind=c_char), intent(in)        :: msg
        end subroutine lual_checkstack

        !> @brief Checks whether the function argument `arg` has type `t`.
        !!
        !! See `lua_type` for the encoding of types for `t`.
        !!
        !! C signature: `void luaL_checktype (lua_State *L, int arg, int t)`
        subroutine lual_checktype(l, arg, t) bind(c, name='luaL_checktype')
            import :: c_ptr, c_int
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Position of function argument in argument list
            integer(kind=c_int),    intent(in), value :: arg
            !> Lua type constant (e.g. `LUA_TTABLE`)
            integer(kind=c_int),    intent(in), value :: t
        end subroutine lual_checktype

        !> @brief Checks whether the function argument `arg` is a userdata
        !! of the type `tname` (see `luaL_newmetatable`) and returns the
        !! userdata address (see `lua_touserdata`).
        !!
        !! C signature: `void *luaL_checkudata_(lua_State *L, int arg, const char *tname)`
        function lual_checkudata(l, arg, tname) bind(c, name='luaL_checkudata')
            import :: c_ptr, c_int, c_char
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Position of function argument in argument list
            integer(kind=c_int),    intent(in), value :: arg
            !> Name of userdata type; see `lua_touserdata`
            character(kind=c_char), intent(in)        :: tname

            ! Return value
            type(c_ptr)                               :: lual_checkudata
        end function lual_checkudata

        !***** C macro; underlying function may not be accessible. Also
        !***** needs constants  `LUA_VERSION_NUM` and `LUAL_NUMSIZES`

        ! !> @brief Checks whether the core running the call, the core
        ! !! that created the Lua state, and the code making the call are
        ! !! all using the same version of Lua. Also checks whether the
        ! !! core running the call and the core that created the Lua state
        ! !! are using the same address space.
        ! !!
        ! !! C signature: `void luaL_checkversion (lua_State *L)`
        ! subroutine lual_checkversion(l) bind(c, name='luaL_checkversion')
        !     import :: c_ptr
        !     !> Pointer to Lua interpreter state
        !     type(c_ptr),         intent(in), value :: l
        ! end subroutine lual_checkversion

        !> @brief  Raises an error.
        !!
        !! The error message format is given by `fmt` plus any extra
        !! arguments, following the same rules of `lua_pushfstring`.
        !! It also adds at the beginning of the message the file name
        !! and the line number where the error occurred, if this
        !! information is available.
        !!
        !! This function never returns, but it is an idiom to use it
        !! in C functions as `return luaL_error(args)`.
        !!
        !! @note The Fortran binding does not support variadic
        !! (multiple optional) arguments so treat `fmt` as static
        !! text rather than as an `fstring` template to be evaluated.
        !!
        !! C signature: `int luaL_error (lua_State *L, const char *fmt, ...)`
        function lual_error(l, fmt) bind(c, name='luaL_error')
            import :: c_ptr, c_int, c_char
            !> Pointer to Lua interpreter state
            type(c_ptr),                   intent(in), value :: l
            !> Error message
            character(kind=c_char),        intent(in)        :: fmt

            ! Return value
            integer(kind=c_int)                              :: lual_error
        end function lual_error

        !> @brief This function produces the return values for
        !! process-related functions in the standard library
        !! (`os.execute` and `io.close`).
        !!
        !! C signature: `int luaL_execresult (lua_State *L, int stat)`
        function lual_execresult(l, stat) bind(c, name='luaL_execresult')
            import :: c_ptr, c_int
            !> Pointer to Lua interpreter state
            type(c_ptr),         intent(in), value :: l
            !> Status code?
            integer(kind=c_int), intent(in), value :: stat

            ! Return value
            integer(kind=c_int)                    :: lual_execresult
        end function lual_execresult

        !> @brief This function produces the return values for
        !! file-related functions in the standard library (`io.open`,
        !! `os.rename`, `file.seek`, etc.).
        !!
        !! C signature: `int luaL_fileresult (lua_State *L, int stat, const char *fname)`
        function lual_fileresult(l, stat, fname) bind(c, name='luaL_fileresult')
            import :: c_ptr, c_int, c_char
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Status code?
            integer(kind=c_int),    intent(in), value :: stat
            !> Error message
            character(kind=c_char), intent(in)        :: fname

            ! Return value
            integer(kind=c_int)                       :: lual_fileresult
        end function lual_fileresult

        !> @brief Pushes onto the stack the field `e` from the metatable
        !! of the object at index `obj` and returns the type of the
        !! pushed value.
        !!
        !! If the object does not have a metatable, or if the metatable
        !! does not have this field, pushes nothing and returns `LUA_TNIL`.
        !!
        !! C signature: `int luaL_getmetafield (lua_State *L, int obj, const char *e)`
        function lual_getmetafield(l, obj, e) bind(c, name='luaL_getmetafield')
            import :: c_ptr, c_char, c_int
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Object index on stack
            integer(kind=c_int),    intent(in), value :: obj
            !> Metatable field name
            character(kind=c_char), intent(in)        :: e

            ! Return value
            integer(kind=c_int)                       :: lual_getmetafield
        end function lual_getmetafield

        !> @brief Pushes onto the stack the metatable associated with
        !! name `tname` in the registry (see `luaL_newmetatable`)
        !! (`nil` if there is no metatable associated with that name).
        !!
        !! Returns the type of the pushed value.
        !!
        !! C signature: `int luaL_getmetatable (lua_State *L, const char *tname)`
        function lual_getmetatable(l, tname) bind(c, name='luaL_getmetatable')
            import :: c_ptr, c_int, c_char
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Metatable field name
            character(kind=c_char), intent(in)        :: tname

            ! Return value
            integer(kind=c_int)                       :: lual_getmetatable
        end function lual_getmetatable

        !> @brief Ensures that the value `t[fname]`, where `t` is the
        !! value at index `idx`, is a table, and pushes that table onto
        !! the stack.
        !!
        !! Returns true if it finds a previous table there and false if
        !! it creates a new table.
        !!
        !! C signature: `int luaL_getsubtable (lua_State *L, int idx, const char *fname)`
        function lual_getsubtable(l, idx, fname) bind(c, name='luaL_getsubtable')
            import :: c_ptr, c_int, c_char
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Object index on stack
            integer(kind=c_int),    intent(in), value :: idx
            !> Name of field containing subtable
            character(kind=c_char), intent(in)        :: fname

            ! Return value
            integer(kind=c_int)                       :: lual_getsubtable
        end function lual_getsubtable

        !> @brief Opens all standard Lua libraries into the given state.
        !!
        !! C signature: `void luaL_openlibs(lua_State *L)`
        subroutine lual_openlibs(l) bind(c, name='luaL_openlibs')
            import :: c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr), intent(in), value :: l
        end subroutine lual_openlibs

        !> @brief Sets the metatable of the object at the top of the
        !! stack as the metatable associated with name `tname` in the
        !! registry (see `luaL_newmetatable`).
        !!
        !! C signature: `void luaL_setmetatable (lua_State *L, const char *tname)`
        subroutine lual_setmetatable(l, tname) bind(c, name='lua_setmetatable')
            import :: c_char, c_int, c_ptr
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Registry key name
            character(kind=c_char), intent(in)        :: tname
        end subroutine lual_setmetatable

        !> @brief This function works like `luaL_checkudata`, except
        !! that, when the test fails, it returns `NULL` instead of
        !! raising an error.
        !!
        !! C signature: `void *luaL_testudata (lua_State *L, int arg, const char *tname)`
        function lual_testudata(l, arg, tname) bind(c, name='luaL_testudata')
            import :: c_ptr, c_int, c_char
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Position of function argument in argument list
            integer(kind=c_int),    intent(in), value :: arg
            !> Name of userdata type; see `lua_touserdata`
            character(kind=c_char), intent(in)        :: tname

            ! Return value
            type(c_ptr)                               :: lual_testudata
        end function lual_testudata

        !> @brief Converts any Lua value at the given index to a C
        !! string in a reasonable format.
        !!
        !! The resulting string is pushed onto the stack and also
        !! returned by the function. If `len` is not `NULL`, the
        !! function also sets `*len` with the string length.
        !!
        !! If the value has a metatable with a `__tostring` field, then
        !! `luaL_tolstring` calls the corresponding metamethod with the
        !! value as argument, and uses the result of the call as its
        !! result.
        !!
        !! C signature: `const char *luaL_tolstring (lua_State *L, int idx, size_t *len)`
        function lual_tolstring_(l, idx, len) bind(c, name='luaL_tolstring')
            import :: c_ptr, c_int, c_char, c_size_t
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Object index on stack
            integer(kind=c_int),    intent(in), value :: idx
            !> String length
            integer(kind=c_size_t), intent(inout)     :: len

            ! Return value
            type(c_ptr)                               :: luaL_tolstring_
        end function luaL_tolstring_

        !> @brief Creates and pushes a traceback of the stack `L1`.
        !!
        !! If `msg` is not `NULL` it is appended at the beginning of the
        !! traceback. The `level` parameter tells at which level to start
        !! the traceback.
        !!
        !! C signature: `void luaL_traceback (lua_State *L, lua_State *L1, const char *msg, int level)`
        subroutine luaL_traceback(l, l1, msg, level) bind(c, name='luaL_traceback')
            import :: c_ptr, c_int, c_char
            !> Pointer to Lua interpreter state
            type(c_ptr),            intent(in), value :: l
            !> Pointer to Lua interpreter state to be traced
            type(c_ptr),            intent(in), value :: l1
            !> Traceback prefix message
            character(kind=c_char), intent(in)        :: msg
            !> Object index on stack
            integer(kind=c_int),    intent(in), value :: level
        end subroutine luaL_traceback

    end interface
    !!!@}
contains
    !> @name Fortran wrapper functions

    !!!@{

    ! !> @brief Returns a pointer to a raw memory area associated with
    ! !! the given Lua state.
    ! !!
    ! !! The application can use this area for any purpose; Lua does
    ! !! not use it for anything.
    ! !!
    ! !! Each new thread has this area initialized with a copy of the
    ! !! area of the main thread.
    ! !!
    ! !! By default, this area has the size of a pointer to `void`,
    ! !! but you can recompile Lua with a different size for this
    ! !! area. (See `LUA_EXTRASPACE` in `luaconf.h`.)
    ! !!
    ! !! @note This is implemented as a C macro:
    ! !!
    ! !!      #define lua_getextraspace(L)       ((void *)((char *)(L) - LUA_EXTRASPACE))
    ! !!
    ! !! @note The constant `LUA_EXTRASPACE` is not exported from the Lua C library
    ! !!
    ! !! C signature: `void *lua_getextraspace (lua_State *L)'
    ! function lua_getextraspace(l)
    !     !> Pointer to Lua interpreter state
    !     type(c_ptr), intent(in) :: l
    !     ! Return value
    !     type(c_ptr)             :: lua_getextraspace
    !     continue
    !     ! Not implemented
    !     return
    ! end function lua_getextraspace

    !> @brief Compares two Lua values.
    !!
    !! Returns 1 if the value at index
    !! `index1` satisfies `op` when compared with the value at index
    !! `index2`, following the semantics of the corresponding Lua
    !! operator (that is, it may call metamethods). Otherwise
    !! returns `.false.`. Also returns `.false.` if any of the indices
    !! is not valid.
    !!
    !! The value of op must be one of the following constants:
    !!  * `LUA_OPEQ`: compares for equality (`==`)
    !!  * `LUA_OPLT`: compares for less than (`<`)
    !!  * `LUA_OPLE`: compares for less or equal (`<=`)
    !!
    !! @note This is a Fortran wrapper around the C binding to `lua_compare`
    !!       which converts the indices to `c_int` and returns a Fortran
    !!       logical (`.true.`/`.false.`) instead of 1/0.
    !!
    !! C signature: `int lua_compare_(lua_State *L, int index1, int index2, int op)`
    function lua_compare(l, index1, index2, op)
        !> Pointer to Lua interpreter state
        type(c_ptr),         intent(in) :: l
        !> Index of first value in comparison
        integer,             intent(in) :: index1
        !> Index of second value in comparison
        integer,             intent(in) :: index2
        !> Comparison operator (one of `LUA_OPEQ`, `LUA_OPLT`, or `LUA_OPLE`)
        integer(kind=c_int), intent(in) :: op
        ! Return value
        logical                         :: lua_compare
        continue

        lua_compare = (lua_compare_(l, int(index1, c_int),              &
            int(index2, c_int), op) /= 0_c_int)

        return
    end function lua_compare

    !> @brief Pushes onto the stack the value of the global name.
    !! Returns the type of that value.
    !!
    !! Wrapper for `lua_getglobal_()` that null-terminates string `name`.
    !!
    !! `int lua_getglobal(lua_State *L, const char *name)`
    function lua_getglobal(l, name)
        !> Pointer to Lua interpreter state
        type(c_ptr),      intent(in) :: l
        !> Name to associate with value
        character(len=*), intent(in) :: name
        ! Return value
        integer                      :: lua_getglobal
        continue

        lua_getglobal = lua_getglobal_(l, name // c_null_char)

        return
    end function lua_getglobal

    !!!@{
    !> @brief Pushes onto the stack the value `t[k]`, where `t` is the
    !! value (table) at the given index. Returns the type of the pushed
    !! value (`t[k]`).
    !!
    !! As in Lua, this function may trigger a metamethod for the
    !! "index" event.
    !!
    !! Wrapper for `lua_getfield_()` that null-terminates string `name`.
    !!
    !! C signature: `int lua_getfield(lua_State *L, int index, const char *name)`
    function lua_getfield(l, idx, name)
        !> Pointer to Lua interpreter state
        type(c_ptr),      intent(in) :: l
        !> Index of table
        integer,          intent(in) :: idx
        !> Name of table key
        character(len=*), intent(in) :: name
        ! Return value
        integer                      :: lua_getfield
        continue

        lua_getfield = lua_getfield_(l, idx, name // c_null_char)

        return
    end function lua_getfield

    !> @brief Moves the top element into the given valid index,
    !! shifting up the elements above this index to open space.
    !!
    !! This function cannot be called with a pseudo-index, because
    !! a pseudo-index is not an actual stack position.
    !!
    !! C signature: `void lua_insert (lua_State *L, int index)`
    subroutine lua_insert(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        continue

        call lua_rotate(l, int(idx, kind=c_int), 1_c_int)

        return
    end subroutine lua_insert

    !> @brief Macro replacement that returns whether the stack variable
    !! is boolean.
    !!
    !! C signature: `int lua_isboolean(lua_State *L, int index)`
    function lua_isboolean(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_isboolean
        continue

        lua_isboolean = (lua_type(l, idx) == LUA_TBOOLEAN)

        return
    end function lua_isboolean

    !> @brief Macro replacement that returns whether the stack variable
    !! is a C function.
    !!
    !! C signature: `int lua_iscfunction(lua_State *L, int idx)`
    function lua_iscfunction(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_iscfunction
        continue

        lua_iscfunction = (lua_iscfunction_(l, idx) == 1)

        return
    end function lua_iscfunction

    !> @brief Macro replacement that returns whether the stack variable
    !! is a function.
    !!
    !! C signature: `int lua_isfunction(lua_State *L, int index)`
    function lua_isfunction(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_isfunction
        continue

        lua_isfunction = (lua_type(l, idx) == LUA_TFUNCTION)

        return
    end function lua_isfunction

    !> @brief Macro replacement that returns whether the stack variable
    !! is an integer.
    !!
    !! C signature: `int lua_isinteger(lua_State *L, int idx)`
    function lua_isinteger(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_isinteger
        continue

        lua_isinteger = (lua_isinteger_(l, idx) == 1)

        return
    end function lua_isinteger

    !> @brief Macro replacement that returns whether the stack variable
    !! is a light userdata
    !!
    !! C signature: `int lua_islightuserdata(lua_State *L, int index)`
    function lua_islightuserdata(l, idx)
        !! Macro replacement that returns whether the stack variable is
        !! light user data.

        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_islightuserdata
        continue

        lua_islightuserdata = (lua_type(l, idx) == LUA_TLIGHTUSERDATA)

        return
    end function lua_islightuserdata

    !> @brief Macro replacement that returns whether the stack variable
    !! is `nil`.
    !!
    !! C signature: `int lua_isnil(lua_State *L, int index)`
    function lua_isnil(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_isnil
        continue

        lua_isnil = (lua_type(l, idx) == LUA_TNIL)

        return
    end function lua_isnil

    !> @brief Macro replacement that returns whether the stack variable
    !! is `none`.
    !!
    !! C signature: `int lua_isnone(lua_State *L, int index)`
    function lua_isnone(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_isnone
        continue

        lua_isnone = (lua_type(l, idx) == LUA_TNONE)

        return
    end function lua_isnone

    !> @brief Macro replacement that returns whether the stack variable
    !! is `none` or `nil`.
    !!
    !! C signature: `int lua_isnoneornil(lua_State *L, int index)`
    function lua_isnoneornil(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx

        ! Return value
        logical                 :: lua_isnoneornil
        continue

        lua_isnoneornil = (lua_type(l, idx) <= 0)

        return
    end function lua_isnoneornil

    !> @brief Macro replacement that returns whether the stack variable
    !! is a number.
    !!
    !! C signature: `int lua_isnumber(lua_State *L, int idx)`
    function lua_isnumber(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_isnumber
        continue

        lua_isnumber = (lua_isnumber_(l, idx) == 1)

        return
    end function lua_isnumber

    !> @brief Macro replacement that returns whether the stack variable
    !! is a string.
    !!
    !! C signature: `int lua_isstring(lua_State *L, int idx)`
    function lua_isstring(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_isstring
        continue

        lua_isstring = (lua_isstring_(l, idx) == 1)

        return
    end function lua_isstring

    !> @brief Macro replacement that returns whether the stack variable
    !! is a table.
    !!
    !! C signature: `int lua_istable(lua_State *L, int index)`
    function lua_istable(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx

        ! Return value
        logical                 :: lua_istable
        continue

        lua_istable = (lua_type(l, idx) == LUA_TTABLE)

        return
    end function lua_istable

    !> @brief Macro replacement that returns whether the stack variable
    !! is a thread.
    !!
    !! C signature: `int lua_isthread(lua_State *L, int index)`
    function lua_isthread(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx

        ! Return value
        logical                 :: lua_isthread
        continue

        lua_isthread = (lua_type(l, idx) == LUA_TTHREAD)

        return
    end function lua_isthread

    !> @brief Macro replacement that returns whether the stack variable
    !! is a userdata.
    !!
    !! C signature: `int lua_isuserdata(lua_State *L, int idx)`
    function lua_isuserdata(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        ! Return value
        logical                 :: lua_isuserdata
        continue

        lua_isuserdata = (lua_isuserdata_(l, idx) == 1)

        return
    end function lua_isuserdata

    !> @brief Macro replacement that returns whether the given coroutine
    !! can yield.
    !!
    !! C signature: `int lua_isyieldable(lua_State *L)`
    function lua_isyieldable(l)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        ! Return value
        logical                 :: lua_isyieldable
        continue

        lua_isyieldable = (lua_isyieldable_(l) == 1)

        return
    end function lua_isyieldable

    !> @brief Creates a new empty table and pushes it onto the stack.
    !! Replaces macro implementation.
    !!
    !! `lua_newtable(L)` is equivalent to `lua_createtable(L, 0, 0)`.
    !!
    !! C signature: `void lua_newtable(lua_State *L)`
    subroutine lua_newtable(l)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        continue

        call lua_createtable(l, 0, 0)

        return
    end subroutine lua_newtable

    !> @brief Converts a Lua float to a Lua integer.
    !!
    !! This macro assumes that `n` has an integral value. If that
    !! value is within the range of Lua integers, it is converted to
    !! an integer and assigned to `*p`. The macro results in a
    !! boolean indicating whether the conversion was successful.
    !!
    !! It is probably best to use Fortran's `int()` intrinsic
    !! instead, though it doesn't restrict range or return a logical
    !! value (false = overflow)
    !!
    !! @note This range test can be tricky to do correctly without
    !! this macro, due to roundings.
    !!
    !! C signature: `int lua_numbertointeger (lua_Number n, lua_Integer *p)`
    ! /*
    ! @@ lua_numbertointeger converts a float number to an integer, or
    ! ** returns 0 if float is not within the range of a lua_Integer.
    ! ** (The range comparisons are tricky because of rounding. The tests
    ! ** here assume a two-complement representation, where MININTEGER always
    ! ** has an exact representation as a float; MAXINTEGER may not have one,
    ! ** and therefore its conversion to float may have an ill-defined value.)
    ! */
    ! #define lua_numbertointeger(n,p) \
    !   ((n) >= (LUA_NUMBER)(LUA_MININTEGER) && \
    !    (n) < -(LUA_NUMBER)(LUA_MININTEGER) && \
    !       (*(p) = (LUA_INTEGER)(n), 1))        !!
    function lua_numbertointeger(n, i)
        !> Real number to convert
        real(kind=c_lua_number),     intent(in)  :: n
        !> Destination integer
        integer(kind=c_lua_integer), intent(out) :: i

        ! Return value
        logical                                  :: lua_numbertointeger
        continue

        lua_numbertointeger = (abs(n) < real(huge(i), kind=c_lua_number))
        if (lua_numbertointeger) then
            i = int(n, kind=c_lua_integer)
        else
            i = 0_c_lua_integer
        end if

        return
    end function lua_numbertointeger

    !> @brief Calls a function in protected mode
    !!
    !! Both `nargs` and `nresults` have the same meaning as in
    !! `lua_call`. If there are no errors during the call,
    !! `lua_pcallk` behaves exactly like `lua_callk`. However, if
    !! there is any error, `lua_pcallk` catches it, pushes a single
    !! value on the stack (the error object), and returns an error
    !! code. Like `lua_call`, `lua_pcallk` always removes the
    !! function and its arguments from the stack.
    !!
    !! If `msgh` is 0, then the error object returned on the stack
    !! is exactly the original error object. Otherwise, `msgh` is
    !! the stack index of a message handler. (This index cannot be a
    !! pseudo-index.) In case of runtime errors, this function will
    !! be called with the error object and its return value will be
    !! the object returned on the stack by `lua_pcallk`.
    !!
    !! Typically, the message handler is used to add more debug
    !! information to the error object, such as a stack traceback.
    !! Such information cannot be gathered after the return of
    !! `lua_pcallk`, since by then the stack has unwound.
    !!
    !! The `lua_pcallk` function returns one of the following
    !! constants (defined in `lua.h`):
    !!   * `LUA_TH_OK` (0): success.
    !!   * `LUA_TH_ERRRUN`: a runtime error.
    !!   * `LUA_TH_ERRMEM`: memory allocation error. For such errors,
    !!     Lua does not call the message handler.
    !!   * `LUA_TH_ERRERR`: error while running the message handler.
    !!   * `LUA_TH_ERRGCMM`: error while running a `__gc` metamethod.
    !!     For such errors, Lua does not call the message handler
    !!     (as this kind of error typically has no relation with the
    !!     function being called).
    !!
    !! @note This function replaces a C macro that calls `lua_pcallk()`.
    !!
    !! C signature: `int lua_pcall(lua_State *L, int nargs, int nresults, int msgh)`
    !!
    !! @note Continuation-function context is hard-coded to `0_c_lua_integer`
    !! (64-bit integer) which may cause problems if `LUA_INTEGER` is
    !! defined to anything other than `long long` in `luaconf.h`
    function lua_pcall(l, nargs, nresults, msgh)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Number of arguments
        integer,     intent(in) :: nargs
        !> Maximum number of results to return
        integer,     intent(in) :: nresults
        !> Pointer to message handler
        integer,     intent(in) :: msgh

        ! Return value
        integer                 :: lua_pcall
        continue

        ! lua_pcall = lua_pcallk(l, nargs, nresults, msgh, int(0, kind=8), c_null_ptr)
        ! lua_pcall = lua_pcallk(l, nargs, nresults, msgh, 0_c_lua_integer, c_null_ptr)
        lua_pcall = lua_pcallk(l, nargs, nresults, msgh, 0_c_int, c_null_funptr)

        return
    end function lua_pcall

    !> @brief Removes the element at the given valid index, shifting
    !! down the elements above this index to fill the gap. This
    !! function cannot be called with a pseudo-index, because a
    !! pseudo-index is not an actual stack position.
    !!
    !! C signature: `void lua_remove (lua_State *L, int index)`
    subroutine lua_remove(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        continue

        call lua_rotate(L, int(idx, kind=c_int), -1_c_int)
        call lua_pop(L, 1_c_int)

        return
    end subroutine lua_remove

    !> @brief Moves the top element into the given valid index
    !! without shifting any element (therefore replacing the value
    !! at that given index), and then pops the top element.
    !!
    !! C signature: `void lua_replace (lua_State *L, int index)`
    subroutine lua_replace(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to check
        integer,     intent(in) :: idx
        continue

        call lua_copy(L, -1_c_int, int(idx, kind=c_int))
        call lua_pop(L, 1_c_int)

        return
    end subroutine lua_replace

    !> @brief Converts the Fortran string `s` to a number, pushes that
   !! number into the stack, and returns the total size of the string,
    !! that is, its length plus one.
    !!
    !! The conversion can result in an integer or a float, according to
    !! the lexical conventions of Lua. The string may have leading and
    !! trailing spaces and a sign. If the string is not a valid numeral,
    !! returns 0 and pushes nothing. (Note that the result can be used
    !! as a boolean, true if the conversion succeeds.)
    !!
    !! @note This wraps the original C function, available at
    !!  `lua_stringtonumber_`. That function takes a null-terminated C
    !!  string instead of a Fortran string.
    !!
    !! C signature: `size_t lua_stringtonumber (lua_State *L, const char *s)`
    function lua_stringtonumber(l, s)
        !> Pointer to Lua interpreter state
        type(c_ptr),      intent(in) :: l
        !> String to convert to a number and push to stack
        character(len=*), intent(in) :: s

        ! Return value
        integer(kind=c_size_t)       :: lua_stringtonumber

        continue

        lua_stringtonumber = lua_stringtonumber_(l, s // c_null_char)

        return
    end function lua_stringtonumber

    !> @brief Converts the Lua value at the given index to a Fortran logical
    !! value. Zero is `false` in C, non-zero is `true`
    !!
    !! C signature: `int lua_toboolean (lua_State *L, int index)`
    !!
    !! @note Fortran wrapper around `lua_toboolean_()` in Lua C API
    function lua_toboolean(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to convert
        integer,     intent(in) :: idx

        ! Return value
        logical                 :: lua_toboolean
        continue

        lua_toboolean = (lua_toboolean_(l, idx) /= 0)

        return
    end function lua_toboolean

    !> @brief Converts the Lua value at the given index to the signed
    !! integral type `lua_Integer`.
    !!
    !! The Lua value must be an integer, or a number or string
    !! convertible to an integer; otherwise, `lua_tointeger` returns 0
    !!
    !! @note Fortran wrapper around `lua_tointegerx()` in Lua C API;
    !! replaces C macro
    !!
    !! C signature: `lua_Integer lua_tointeger(lua_State *l, int idx)`
    function lua_tointeger(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr),       intent(in) :: l
        !> Index of element to convert
        integer,           intent(in) :: idx

        ! Return value
        integer(kind=INT64)           :: lua_tointeger
        continue

        lua_tointeger = int(lua_tointegerx(l, idx, c_null_ptr), INT64)

        return
    end function lua_tointeger

    !> @brief Converts the Lua value at the given index to the signed
    !! real type `lua_Number`.
    !!
    !! The Lua value must be an number or a string convertible to a
    !! number; otherwise, `lua_tonumber` returns 0
    !!
    !! @note Fortran wrapper around `lua_tonumberx()` in Lua C API;
    !! replaces C macro
    !!
    !! C signature: `lua_Number lua_tonumber (lua_State *L, int index)`
    function lua_tonumber(l, idx)
        use, intrinsic :: iso_fortran_env, only: REAL64
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of element to convert
        integer,     intent(in) :: idx

        ! Return value
        real(kind=REAL64)                 :: lua_tonumber
        continue

        lua_tonumber = real(lua_tonumberx(l, idx, c_null_ptr), kind=REAL64)

        return
    end function lua_tonumber

    !> @brief Wrapper that calls `lua_tolstring()` and converts the returned C
    !! pointer to a Fortran string.
    !!
    !! C signature: `const char *lua_tostring(lua_State *L, int index)`
    function lua_tostring(l, idx)
        ! use, intrinsic :: iso_fortran_env, only: INT64

        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in)       :: l
        !> Index of stack element to convert
        integer,     intent(in)       :: idx

        ! Return value
        character(len=:), allocatable :: lua_tostring

        type(c_ptr)                   :: ptr
        integer(kind=c_size_t)        :: size
        continue

        ptr = lua_tolstring(l, idx, c_null_ptr)
        if (.not. c_associated(ptr)) return

        size = c_strlen(ptr)
        allocate (character(len=size) :: lua_tostring)
        call c_f_string_ptr(ptr, lua_tostring)

        return
    end function lua_tostring

    !> @brief Wrapper that calls `lua_typename_()` and converts the
    !! returned C pointer to Fortran string.
    !!
    !! C signature: `const char *lua_typename(lua_State *L, int tp)`
    function lua_typename(l, tp)
        use, intrinsic :: iso_fortran_env, only: INT64
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in)       :: l
        !> Lua value type code
        integer,     intent(in)       :: tp

        ! Return value
        character(len=:), allocatable :: lua_typename

        type(c_ptr)                   :: ptr
        integer(kind=INT64)           :: size
        continue

        ptr = lua_typename_(l, tp)
        if (.not. c_associated(ptr)) return

        size = c_strlen(ptr)
        allocate (character(len=size) :: lua_typename)
        call c_f_string_ptr(ptr, lua_typename)

        return
    end function lua_typename

    !> @brief Converts the Lua value at the given index to the signed
    !! real type `lua_Number`.
    !!
    !! The Lua value must be an number or a string convertible to a
    !! number; otherwise, `lua_tonumber` returns 0
    !!
    !! C signature: `lua_Number lua_tonumber (lua_State *L, int index)`
    !!
    !! @note Fortran wrapper around `lua_tonumberx()` in Lua C API
    function lua_version(l)
        use, intrinsic :: iso_fortran_env, only: REAL64
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        ! Return value
        integer                 :: lua_version

        real(kind=REAL64)       :: tmp_version
        continue

        call c_f_real_ptr(lua_version_(l), tmp_version)
        lua_version = int(tmp_version)

        return
    end function lua_version

    !> @brief  Yields a coroutine (thread) with no continuation.
    !!
    !! This function is equivalent to `lua_yieldk`, but it
    !! has no continuation. Therefore, when the thread resumes, it
    !! continues the function that called the function calling `lua_yield`.
    !!
    !! This function replaces a C macro of the same name
    !!
    !! C signature: `int lua_yield (lua_State *L, int nresults)`
    function lua_yield(l, nresults)
        !> Pointer to Lua interpreter state
        type(c_ptr),              intent(in) :: l
        !> Maximum number of results
        integer(kind=c_int),      intent(in) :: nresults
        ! Return value
        integer(kind=c_int)                  :: lua_yield
        continue

        lua_yield = lua_yieldk(l, nresults, 0_c_int, c_null_funptr)

    end function lua_yield

    ! !> @brief Raises an error reporting a problem with argument
    ! !! `arg` of the C function that called it, using a standard
    ! !! message that includes `extramsg` as a comment:
    ! !!
    ! !!      bad argument #arg to 'funcname' (extramsg)
    ! !!
    ! !! This function never returns.
    ! !!
    ! !! This is a Fortran wrapper around `lua_argerror_`. This version
    ! !! takes `extramsg` as a Fortran string. `arg` is converted to a
    ! !! C integer and `extramsg` is null-terminated for the call to
    ! !! `lua_argerror_` to maintain compatibility with the underlying
    ! !! C API.
    ! !!
    ! !! @note `lual_argerror` is a subroutine, not an integer function.
    ! !! Since `lual_argerror_` never returns, no return value will be
    ! !! seen. The implementation of the original `lua_argcheck_` macro
    ! !! takes advantage of C's use of integers as logical variables and
    ! !! short-circuit logical operators; the integer return value exists
    ! !! only to satisfy type requirements of the C compiler when
    ! !! compiling the macro. The Fortran implementation of `lua_argcheck`
    ! !! does not have to satisfy the type constraint and is more
    ! !! reasonable to invoke it as a subroutine rather than as a
    ! !! function.
    ! !!
    ! !! C signature: `int luaL_argerror (lua_State *L, int arg, const char *extramsg)`
    ! subroutine lual_argerror(l, arg, extramsg)
    !     !> Pointer to Lua interpreter state
    !     type(c_ptr),            intent(in) :: l
    !     !> Position of problematic argument in function argument list
    !     integer,                intent(in) :: arg
    !     !> C format (null terminated) string containing additional error detail
    !     character(len=*),       intent(in) :: extramsg

    !     integer(kind=c_int)                :: dummy
    !     continue

    !     dummy = lual_argerror_(l, int(arg, kind=c_int),                 &
    !         extramsg // c_null_char)

    !     return
    ! end subroutine lual_argerror

    ! !> @brief Checks whether `cond` is true. If it is not, raises an
    ! !! error reporting a problem with argument `arg` of the
    ! !! C function that called it, using a standard message that
    ! !! includes extramsg as a comment:
    ! !!
    ! !!      bad argument #arg to 'funcname' (extramsg)
    ! !!
    ! !! This routine replaces the macro `luaL_argcheck` with a call to
    ! !! the Fortran wrapper around `lual_argerror`. This version takes
    ! !! `cond` as a `LOGICAL` and `extramsg` as a Fortran
    ! !! string. `l`, `arg`, and `extramsg` are passed unmodified to
    ! !! `lual_argerror`
    ! !!
    ! !! C signature: `void luaL_argcheck (lua_State *L, int cond, int arg, const char *extramsg)`
    ! subroutine lual_argcheck(l, cond, arg, extramsg)
    !     !> Pointer to Lua interpreter state
    !     type(c_ptr),            intent(in) :: l
    !     !> Condition true/false value
    !     logical,                intent(in) :: cond
    !     !> Position of problematic argument in function argument list
    !     integer,                intent(in) :: arg
    !     !> String containing additional error detail
    !     character(len=*),       intent(in) :: extramsg

    !     ! integer(kind=c_int)                :: icond

    !     continue

    !     if (cond) then
    !         call lual_argerror(l, arg, extramsg)
    !     end if

    !     return
    ! end subroutine lual_argcheck

    !> @brief Checks whether the function argument `arg` is a string
    !! and returns this string; if `l` is not `NULL` fills `*l` with
    !! the string's length.
    !!
    !! This function uses `lua_tolstring` to get its result, so all
    !! conversions and caveats of that function apply here.
    !!
    !! @note This is a Fortran wrapper which converts input and output
    !! integers from Fortran to C types and converts the return value
    !! C string pointer to a Fortran string.
    !!
    !! C signature: `const char *luaL_checklstring (lua_State *L, int arg, size_t *l)`
    function lual_checklstring(l, idx, len)
        !> Pointer to Lua interpreter state
        type(c_ptr),         intent(in)    :: l
        !> Index of function argument to check
        integer,             intent(in)    :: idx
        !> Index of function argument to check
        integer,             intent(inout) :: len

        ! Return value
        character(len=:), allocatable          :: lual_checklstring

        integer(kind=c_size_t)                 :: dumlen
        type(c_ptr)                            :: p2cstr
        continue

        p2cstr = lual_checklstring_(l, int(idx, kind=c_int), dumlen)
        call c_f_string_ptr(p2cstr, lual_checklstring)
        len = int(dumlen)

        return
    end function lual_checklstring

    !> @brief Checks whether the function argument `arg` is a number
    !! and returns this number.
    !!
    !! @note This function takes and returns Fortran arguments,
    !! performing C type coversion before and after calling;
    !! the wrapper function `lual_checknumber_`
    !!
    !! C signature: `lua_Number luaL_checknumber (lua_State *L, int arg)`
    function lual_checknumber(l, arg)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Index of function argument to check
        integer,     intent(in) :: arg

        ! Return value
        real(kind=REAL64)       :: lual_checknumber
        integer(kind=c_int)     :: iarg

        continue

        iarg = int(arg, kind=c_int)
        lual_checknumber = real(lual_checknumber_(l, iarg), kind=REAL64)

        return
    end function lual_checknumber

    !> @brief Checks whether the function argument `arg` is a string
    !! and returns this string.
    !!
    !! This function uses `lua_tolstring` to get its result, so all
    !! conversions and caveats of that function apply here.
    !!
    !! @note This is a Fortran wrapper passes its arguments directly
    !! to `lual_checkstring` which does all the C type converstion
    !! before and after calling the C auxiliary function
    !! `lual_checkstring_`
    !!
    !! C signature: `const char *luaL_checkstring (lua_State *L, int arg)`
    function lual_checkstring(l, idx)
        !> Pointer to Lua interpreter state
        type(c_ptr),         intent(in), value :: l
        !> Index of function argument to check
        integer,             intent(in)        :: idx

        ! Return value
        character(len=:), allocatable          :: lual_checkstring

        integer                                :: dumlen

        continue

        lual_checkstring = lual_checklstring(l, idx, dumlen)

        return
    end function lual_checkstring

    !> @brief Loads and runs the given file.
    !!
    !! Macro replacement that calls `lual_loadfile()` and `lua_pcall()`.
    !!
    !! C signature: `int luaL_dofile(lua_State *L, const char *filename)`
    function lual_dofile(l, fn)
        !> Pointer to Lua interpreter state
        type(c_ptr),      intent(in) :: l
        !> File name of Lua chunk
        character(len=*), intent(in) :: fn
        ! Return value
        integer                      :: lual_dofile
        continue

        lual_dofile = lual_loadfile(l, fn)

        if (lual_dofile == 0) then
            lual_dofile = lua_pcall(l, 0, LUA_TH_MULTRET, 0)
        end if

        return
    end function lual_dofile

    !> @brief Loads and runs the given file.
    !!
    !! Macro replacement that calls `lual_loadstring()` and `lua_pcall()`.
    !!
    !! C signature: `int luaL_dostring (lua_State *L, const char *str)`
    function lual_dostring(l, s)
        !> Pointer to Lua interpreter state
        type(c_ptr),      intent(in) :: l
        !> String to load as a Lua chunk
        character(len=*), intent(in) :: s
        ! Return value
        integer                      :: lual_dostring

        continue

        lual_dostring = luaL_loadstring(l, s)
        if (lual_dostring == 0) then
            lual_dostring = lua_pcall(l, 0, LUA_TH_MULTRET, 0)
        end if

        return
    end function lual_dostring

    !***** Needs access to (lua.h) constant LUA_REGISTRY_INDEX which is
    !***** not exported

    ! !> @brief Pushes onto the stack the metatable associated with name
    ! !! `tname` in the registry (see `luaL_newmetatable`) (`nil` if there
    ! !! is no metatable associated with that name). Returns the type of
    ! !! the pushed value.
    ! !!
    ! !! @note Originally implemented as a C macro in `lauxlib.h`
    ! !!
    ! !! C signature: `int luaL_getmetatable (lua_State *L, const char *tname)`
    ! function lual_getmetatable(l, tname)
    !     !> Pointer to Lua interpreter state
    !     type(c_ptr),      intent(in) :: l
    !     !> File name of Lua chunk
    !     character(len=*), intent(in) :: tname
    !     ! Return value
    !     integer                      :: lual_getmetatable
    !     continue

    !     lual_getmetatable = lual_getfield(l, LUA_REGISTRY_INDEX,        &
    !         tname // c_null_char)

    !     return
    ! end function lual_getmetatable

    !> @brief Macro replacement that calls `lual_loadfilex()`.
    !!
    !! C signature: `int luaL_loadfile(lua_State *L, const char *filename)`
    function lual_loadfile(l, fn)
        !> Pointer to Lua interpreter state
        type(c_ptr),      intent(in) :: l
        !> File name of Lua chunk
        character(len=*), intent(in) :: fn
        ! Return value
        integer                      :: lual_loadfile
        continue

        lual_loadfile = lual_loadfilex(l, fn // c_null_char, c_null_ptr)

        return
    end function lual_loadfile

    !> Wrapper for `lual_loadstring()` that null-terminates the given
    !! string.
    !!
    !! C signature: `int luaL_loadstring(lua_State *L, const char *s)`
    function lual_loadstring(l, s)
        !> Pointer to Lua interpreter state
        type(c_ptr),      intent(in) :: l
        !> String to load as a Lua chunk
        character(len=*), intent(in) :: s
        ! Return value
        integer                      :: lual_loadstring
        continue

        lual_loadstring = lual_loadstring_(l, s // c_null_char)

        return
    end function lual_loadstring

    !> @brief Returns the name of the type of the value at the given index.
    !!
    !! C signature: `const char *luaL_typename (lua_State *L, int index)`
    function lual_typename(l, idx)
        use, intrinsic :: iso_fortran_env, only: INT64
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in)       :: l
        !> Stack index
        integer,     intent(in)       :: idx

        ! Lua value type code
        integer                       :: tp

        ! Return value
        character(len=:), allocatable :: lual_typename

        type(c_ptr)                   :: ptr
        integer(kind=INT64)           :: size
        continue

        tp = lua_type(l, idx)
        ptr = lua_typename_(l, tp)
        if (.not. c_associated(ptr)) return

        size = c_strlen(ptr)
        allocate (character(len=size) :: lual_typename)
        call c_f_string_ptr(ptr, lual_typename)

        return
    end function lual_typename

    !> @brief Converts any Lua value at the given index to a C
    !! string in a reasonable format.
    !!
    !! The resulting string is pushed onto the stack and also
    !! returned by the function. If `len` is not `NULL`, the
    !! function also sets `*len` with the string length.
    !!
    !! If the value has a metatable with a `__tostring` field, then
    !! `luaL_tolstring` calls the corresponding metamethod with the
    !! value as argument, and uses the result of the call as its
    !! result.
    !!
    !! @note This is a Fortran wrapper which converts input and output
    !! integers from Fortran to C types and converts the return value
    !! C string pointer to a Fortran string.
    !!
    !! C signature: `const char *luaL_tolstring (lua_State *L, int idx, size_t *len)`
    function lual_tolstring(l, idx, len)
        !> Pointer to Lua interpreter state
        type(c_ptr),         intent(in)    :: l
        !> Index of function argument to check
        integer,             intent(in)    :: idx
        !> Index of function argument to check
        integer,             intent(inout) :: len

        ! Return value
        character(len=:), allocatable          :: lual_tolstring

        integer(kind=c_size_t)                 :: dumlen
        type(c_ptr)                            :: p2cstr

        continue

        p2cstr = lual_tolstring_(l, int(idx, kind=c_int), dumlen)
        len = int(dumlen)
        allocate(character(len * 2) :: lual_tolstring)
        call c_f_string_ptr(p2cstr, lual_tolstring)

        return
    end function lual_tolstring

    !> @brief Calls a function. Fortran wrapper around `lua_callk`
    !!
    !! C signature: `void lua_call(lua_State *L, int nargs, int nresults)`
    !!
    !! @note Continuation-function context is hard-coded to `0_c_lua_integer`
    !! (64-bit integer) which may cause problems if `LUA_INTEGER` is
    !! defined to anything other than `__int64` in `luaconf.h`
    subroutine lua_call(l, nargs, nresults)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Number of arguments
        integer,     intent(in) :: nargs
        !> Maximum number of results
        integer,     intent(in) :: nresults
        continue

        ! call lua_callk(l, nargs, nresults, int(0, kind=8), c_null_ptr)
        ! call lua_callk(l, nargs, nresults, 0_c_lua_integer, c_null_ptr)
        call lua_callk(l, nargs, nresults, 0_c_int, c_null_funptr)

        return
    end subroutine lua_call

    !> @brief Pops `n` elements from the stack.
    !!
    !! @note Fortran wrapper around `lua_settop`; replaces C macro.
    !!
    !! C signature: `void lua_pop(lua_State *l, int n)
    subroutine lua_pop(l, n)
        !> Pointer to Lua interpreter state
        type(c_ptr), intent(in) :: l
        !> Number of stack elements to pop
        integer,     intent(in) :: n
        continue

        call lua_settop(l, -n - 1)

        return
    end subroutine lua_pop

    !> @brief Pushes a C function onto the stack.
    !!
    !! This function receives a pointer to a C function and pushes onto
    !! the stack a Lua value of type function that, when called, invokes
    !! the corresponding C function.
    !!
    !! Any function to be callable by Lua must follow the correct
    !! protocol to receive its parameters and return its results
    !! (see lua_CFunction).
    !!
    !! @note Fortran wrapper around `lua_pushcclosure`; replaces C macro
    !!
    !! C signature: `void lua_pushcfunction(lua_State *L, lua_CFunction f)`
    subroutine lua_pushcfunction(l, f)
        !> Pointer to Lua interpreter state
        type(c_ptr),    intent(in) :: l
        !> Pointer to C function
        type(c_funptr), intent(in) :: f
        continue

        call lua_pushcclosure(l, f, 0)

        return
    end subroutine lua_pushcfunction

    !***** MACRO; cannot be implemented without having values for
    !***** LUA_REGISTRYINDEX and LUA_RIDX_GLOBALS

    ! !> @brief Pushes the global environment onto the stack. Wrapper
    ! !! around `lua_rawgeti`
    ! !!
    ! !! Original C macro: `((void)lua_rawgeti(L, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS))`
    ! !!
    ! !! C signature: `void lua_pushglobaltable (lua_State *L)`
    ! subroutine lua_pushglobaltable(l)
    !     !> Pointer to Lua interpreter state
    !     type(c_ptr),            intent(in), value :: l
    !     continue

    !     call lua_rawgeti(l, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS)

    !     return
    ! end subroutine lua_pushglobaltable

    !> @brief This function is equivalent to `lua_pushstring`, but
    !! should be used only when `s` is a literal string.
    !!
    !! @note Fortran wrapper around `lua_pushstring`; replaces C macro
    !!
    !! C signature: `const char *lua_pushliteral (lua_State *L, const char *s)`
    function lua_pushliteral(l, s)
        !> Pointer to Lua interpreter state
        type(c_ptr),            intent(in) :: l
        !> String to push onto stack
        character(kind=c_char), intent(in) :: s
        ! Return value
        type(c_ptr)                        :: lua_pushliteral

        continue

        lua_pushliteral = lua_pushstring(l, s)

        return
    end function lua_pushliteral

    !> @brief Sets the C function f as the new value of global `name`.
    !!
    !! C signature: `void lua_register(lua_State *L, const char *name, lua_CFunction f)`
    subroutine lua_register(l, n, f)
        !> Pointer to Lua interpreter state
        type(c_ptr),      intent(in) :: l
        !> Name for registering function `f`
        character(len=*), intent(in) :: n
        !> Pointer to function
        type(c_funptr),   intent(in) :: f
        continue

        call lua_pushcfunction(l, f)
        call lua_setglobal(l, n // c_null_char)

        return
    end subroutine lua_register

    !> @brief Does the equivalent to `t[k] = v`, where `t` is the
    !! value (table) at the given index and `v` is the value at the
    !! top of the stack.
    !!
    !! This function pops the value from the stack. As in Lua, this
    !! function may trigger a metamethod for the "newindex" event
    !!
    !! @note This is a wrapper around `lua_setfield_()`
    !!
    !! C signature: `void lua_setfield (lua_State *L, int index, const char *k);`
    subroutine lua_setfield(l, idx, name)
        !> Pointer to Lua interpreter state
        type(c_ptr),      intent(in) :: l
        !> Index of table
        integer,          intent(in) :: idx
        !> Name of table key
        character(len=*), intent(in) :: name
        continue

        call lua_setfield_(l, idx, name // c_null_char)

        return
    end subroutine lua_setfield
    !!!@}

    !> @name C/Fortran convenience functions

    !!!@{
    !> @brief Utility routine that copies a C string, passed as
    !! a C pointer, to a Fortran string.
    subroutine c_f_string_ptr(c_string, f_string)
        !> Pointer to C string
        type(c_ptr),      intent(in)           :: c_string
        !> Fortran string
        character(len=*), intent(out)          :: f_string

        character(kind=c_char, len=1), pointer :: char_ptrs(:)
        integer                                :: i
        continue

        if (.not. c_associated(c_string)) then
            f_string = ' '
        else
            call c_f_pointer(c_string, char_ptrs, [huge(0)])

            i = 1

            do while (char_ptrs(i) /= c_null_char .and. i <= len(f_string))
                f_string(i:i) = char_ptrs(i)
                i = i + 1
            end do

            if (i < len(f_string)) &
                f_string(i:) = ' '
        end if

        return
    end subroutine c_f_string_ptr

    !> @brief Utility routine that copies a Lua_Number, passed as
    !! a C pointer, to a Fortran real(double).
    subroutine c_f_real_ptr(c_real, f_real)
        use, intrinsic :: iso_fortran_env, only: REAL64
        !> Pointer to C double (Lua_Number)
        type(c_ptr),      intent(in)           :: c_real
        !> Fortran string
        real(kind=REAL64), intent(out)         :: f_real

        real(kind=c_lua_number), pointer       :: ptr_to_real
        continue

        f_real = 0.0_REAL64
        if (c_associated(c_real)) then
            call c_f_pointer(c_real, ptr_to_real)
            f_real = real(ptr_to_real, kind=REAL64)
        end if

        return
    end subroutine c_f_real_ptr
    !!!@}
end module lua
