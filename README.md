# fortran-lua53
A collection of ISO C binding interfaces to Lua 5.3 for Fortran 2003, to call
Lua from Fortran and vice versa.

Note: [This is a fork](https://gitlab.com/apthorpe/fortran-lua53) of the [original `fortran-lua53` library from Philipp Engel](https://github.com/interkosmos/fortran-lua53)

Similar projects:

* [AOTUS](https://geb.sts.nt.uni-siegen.de/doxy/aotus/): Library that provides a Fortran wrapper to use Lua scripts as configuration files (MIT).
* [FortLua](https://github.com/adolgert/FortLua): Example that shows how to load Lua configuration files from Fortran, based on AOTUS (MIT).
* [f2k3-lua](https://github.com/MaikBeckmann/f2k3-lua/tree/simple): Lua bindings for loading configuration files only (MIT).
* [luaf](https://bitbucket.org/vadimz/luaf/): Selected bindings to Lua 5.1 (MIT).

## Dependencies

Before starting, review the list of required and optional dependencies. Note that the original installation instructions should still work; see **Original non-CMake Build Instructions** for `xmake` and manual build guidance.

### Requirements

* A recent Fortran compiler (`gfortran` from GCC9 and GCC10 has been tested)
* A recent C compiler (`gcc` from GCC9 and GCC10 has been tested)
* A recent version of [CMake](https://cmake.org/), preferably 3.17 or later (earlier may work but have not been tested)
* A low-level build tool such as `make` or [`ninja`](https://ninja-build.org/)
* A copy of the `fortran-lua53` [source code](https://gitlab.com/apthorpe/fortran-lua53)
* If network access is limited, the [Lua 5.3 source code](https://www.lua.org/versions.html) otherwise CMake will retrieve and build Lua 5.3.6 source during the build process

### Optional Tools

[Git](https://git-scm.com/) is used for secure file retrieval during
the build process and can automatically download a known working version
of Lua 5.3 source and the TOAST unit testing framework and its
dependencies. Git is used for dependency retrieval directly or via
git submodules (TOAST only). Note that CMake manages dependency retrieval
and uses git for transport. No knowledge of or familiarity with git is
required and git is not required to build or use `fortran-lua53`.

Unit tests use the [TOAST framework](https://github.com/thomasms/toast)
which in turn requires [fork](https://github.com/thomasms/fork)
and [json-fortran](https://github.com/thomasms/json-fortran).
Unit tests are not enabled by default.

To generate formal PDF documentation from the source code and files in
the `userdoc` directory, the following tools are required:

* A recent verion of [Doxygen](https://www.doxygen.nl/index.html), preferably version 1.8.18 or later
* [Graphviz](https://graphviz.org/), used for illustrating call trees
* A recent LaTeX installation; [TexLive](https://tug.org/texlive/) 2020 has been tested on Linux, Windows, and OSX and is known to work.

Documentation generation is a manual step in CMake and no knowledge
or proficiency with Doxygen or LaTeX is required.

If test coverage analysis is desired, `gcov` and `lcov` are supported
under Linux only (MacOS has not been tested and Windows is known not
to work, at least with `gfortran`). Coverage analysis is not enabled
by default.

Finally, CMake supports a number of packaging tools specific to
Linux, Windows, and MacOS. At a minimum, CPack produces a `.zip`
archive on all platforms, `.exe`, `.msi`, or `.nupkg` on Windows,
DragNDrop (`.dmg`) packages on MacOS, and `.deb` and `.rpm` packages
on Linux. Due to the poor portability of Fortran `.mod` files
among compilers even on the same platform, packages may be of
limited use in any but the largest managed IT environments.

## Build

For all platforms (this should work under both a Linux shell
or a Windows command prompt):

```sh
$ mkdir mybuildroot
$ cd mybuildroot
$ git clone https://gitlab.com/apthorpe/fortran-lua53.git
$ cd fortran-lua53
$ mkdir build
$ cd build
$ cmake ..
```

This will take a while as CMake retrieves dependencies and generates build files.

CMake may be given several options to enable features or set preferences:
* `-G "Unix Makefiles"` will generate makefiles suitable for compiling on Linux or MacOS
* `-G Ninja` will generate Ninja build files on any platform Ninja supports
* `cmake -G` will list the supported build file generators.
* `-D ENABLE_UNIT_TESTS:BOOL=ON` to enable unit tests
* `-D ENABLE_COVERAGE:BOOL=ON` to enable unit tests (Linux only)
* `-D ENABLE_DOCUMENTATION:BOOL=OFF` to avoid searching for documentation tools (`ON` by default)
* `-D CMAKE_Fortran_COMPILER:FILEPATH=/path/to/a/compiler/like/gfortran` sets the Fortran compiler to use if CMake cannot determine it otherwise. Equivalent to setting the environment variable `FC`
* `-D CMAKE_C_COMPILER:FILEPATH=/path/to/a/compiler/like/gcc` sets the C compiler to use if CMake cannot determine it otherwise. Equivalent to setting the environment variable `CC`

If CMake generated Unix makefiles,

```sh
$ make
```

If CMake generated Ninja build files,

```sh
$ ninja
```

Running either of these should compile Lua (library, interpreter, and compiler), `fortran-lua53`, and TOAST (if enabled), and compile and run the example programs. Build artifacts are located under `mybuildroot/fortran-lua53/build` - libraries in `build`, Fortran `.mod` files under `build/fortranlua53_include`

To run the example programs and (if enabled) unit tests,

```sh
$ ctest
```

Test results will be under `build/test`. This will work even if unit tests are not enabled but only the example programs will be tested.

To generate documentation, use the local build system, `make docs` or
`ninja docs` as appropriate. **WARNING: THIS WILL TAKE SOME TIME WHICH IS WHY IT IS NOT ENABLED BY DEFAULT.**
PDF documentation will be found at `build/Fortran_Lua53_Users_Guide.pdf`. Note that this file exists whether you generate documentation or not but contains placeholder text if documentation has not been generated.

HTML documentation will be under `build/doc/html` if documentation is built.

To package a minimal set of build artifacts (executables, `.mod` files, libraries) as a `.zip` archive, use:

```sh
$ cpack -G ZIP
```

### Original non-CMake Build Instructions
Install Lua 5.3 with development headers. On FreeBSD, run:

```sh
# pkg install lang/lua53
```

Use [xmake](https://github.com/xmake-io/xmake) to build *fortran-lua53*:

```sh
$ xmake
```

Or, instead, just compile the library manually:

```sh
$ gfortran -c src/lua.f90
$ ar rcs libfortran-lua53.a lua.o
```

Link your Fortran applications statically against `libfortran-lua53.a` and
`liblua-5.3.a`.

## Example
The following basic example shows how to call the Lua function `hello()` in
`script.lua` from Fortran.

```lua
-- script.lua
function hello()
    print('Hello from Lua!')
end
```

Make sure that `script.lua` is stored in the same directory as the Fortran
application.

```fortran
! example.f90
program main
    use, intrinsic :: iso_c_binding, only: c_ptr
    use :: lua
    implicit none
    type(c_ptr) :: l
    integer     :: rc

    l = lual_newstate()
    call lual_openlibs(l)

    rc = lual_dofile(l, 'script.lua')
    rc = lua_getglobal(l, 'hello')
    rc = lua_pcall(l, 0, 0, 0)

    call lua_close(l)
end program main
```

Compile, (dynamically) link, and run the example with:

```sh
$ gfortran -I/usr/local/include/lua53/ -L/usr/local/lib/lua/5.3/ \
  -o example example.f90 libfortran-lua53.a -llua-5.3
$ ./example
Hello from Lua!
```

On Linux, change the prefix `/usr/local` to `/usr`. To link Lua 5.3 statically,
run instead:

```sh
$ gfortran -o example example.f90 libfortran-lua53.a /usr/local/lib/liblua-5.3.a
```

## Further Examples
Additional examples can be found in `examples/`.

* **fibonacci:** calls a recursive Lua routine loaded from file.
* **library:** calls a Fortran routine inside a shared library from Lua. (build with `xmake`; not currently supported by CMake)
* **string:** runs Lua code stored in a Fortran string.

Reviewing the unit tests may also be instructive; see `test/unittest`

## Coverage and Implementation

Note: This table represents the implementation status of [this fork](https://gitlab.com/apthorpe/fortran-lua53)
and not [the original `fortran-lua53` library from Philipp Engel](https://github.com/interkosmos/fortran-lua53).

In the following tables, the **Bound** column indicates there may be a Fortran
interface which directly communicates with the underlying C API function
with no intermediate code or variable type conversion. A check (✓) indicates
an interface exists, an empty column indicates there is no corresponding
interface, **M** indicates the Lua API function is implemented as a C macro
and therefore cannot have an interface, and **w** indicates there is no
interface but a workaround is possible using Fortran and other API functions.

The **Wrapped** column indicates there may be a Fortran function or subroutine
(vs interface) which may implement this API function. Wrapped functions may
perform variable type transformations (e.g. converting between null terminated
C strings and known length Fortran strings) or contain code equivalent to a
corresponding C macro. As above, a check (✓) indicates a Fortran wrapper
function exists, an empty column indicates no wrapper exists.

The main distinction is that bound interfaces may expect arguments as C types
(`c_int`, `c_double`, `c_long_long`, `c_ptr`, `c_funptr`, etc.) while wrapped
functions expect arguments as Fortran types where possible and are associated
with actual program units (functions and subroutines) rather than existing
only as interfaces.

A check in either the Wrapped or the Bound column indicates the API function
is available in Fortran. A check in the **Tested** column indicates there are
unit tests and examples which exercise this function.

The justification and status of unimplemented functions is listed below the
tables describing the full API. Not all unimplemented functions have a
straightforward implementation in Fortran. Some may require additional C
programming to implement (e.g. `lua_dump`). Others seem to have relatively
limited value in a Fortran programming context or application. Consider this a
*best effort* to implement the full Lua 5.3 API. Omissions are explicitly
noted.

### C API Functions

| C API Function Name     | Fortran Interface Name  | Bound | Wrapped | Tested |
|-------------------------|-------------------------|-------|---------|--------|
| `lua_atpanic`           |                         |       |         |        |
| `lua_absindex`          | `lua_absindex`          |   ✓   |         |   ✓    |
| `lua_arith`             | `lua_arith`             |   ✓   |         |   ✓    |
| `lua_call`              | `lua_call`              |   M   |   ✓     |        |
| `lua_callk`             | `lua_callk`             |   ✓   |         |        |
| `lua_checkstack`        | `lua_checkstack`        |   ✓   |         |   ✓    |
| `lua_close`             | `lua_close`             |   ✓   |         |   ✓    |
| `lua_compare`           | `lua_compare`           |   ✓   |   ✓     |   ✓    |
| `lua_concat`            | `lua_concat`            |   ✓   |         |   ✓    |
| `lua_copy`              | `lua_copy`              |   ✓   |         |   ✓    |
| `lua_createtable`       | `lua_createtable`       |   ✓   |         |   ✓    |
| `lua_dump`              |                         |       |         |        |
| `lua_error`             | `lua_error`             |   ✓   |         |   ✓    |
| `lua_gc`                | `lua_gc`                |   ✓   |         |   ✓    |
| `lua_getallocf`         |                         |       |         |        |
| `lua_getextraspace`     |                         |       |         |        |
| `lua_getfield`          | `lua_getfield`          |   ✓   |   ✓     |   ✓    |
| `lua_getglobal`         | `lua_getglobal`         |   ✓   |   ✓     |   ✓    |
| `lua_geti`              | `lua_geti`              |   ✓   |         |   ✓    |
| `lua_getmetatable`      | `lua_getmetatable`      |   ✓   |         |        |
| `lua_gettable`          | `lua_gettable`          |   ✓   |         |        |
| `lua_getuservalue`      | `lua_getuservalue`      |   ✓   |         |        |
| `lua_gettop`            | `lua_gettop`            |   ✓   |         |   ✓    |
| `lua_insert`            | `lua_insert`            |   M   |   ✓     |        |
| `lua_isboolean`         | `lua_isboolean`         |   M   |   ✓     |   ✓    |
| `lua_iscfunction`       | `lua_iscfunction`       |   ✓   |   ✓     |   ✓    |
| `lua_isfunction`        | `lua_isfunction`        |   M   |   ✓     |   ✓    |
| `lua_isinteger`         | `lua_isinteger`         |   ✓   |   ✓     |   ✓    |
| `lua_islightuserdata`   | `lua_islightuserdata`   |   M   |   ✓     |        |
| `lua_isnil`             | `lua_isnil`             |   M   |   ✓     |   ✓    |
| `lua_isnone`            | `lua_isnone`            |   M   |   ✓     |        |
| `lua_isnoneornil`       | `lua_isnoneornil`       |   M   |   ✓     |   ✓    |
| `lua_isnumber`          | `lua_isnumber`          |   ✓   |   ✓     |   ✓    |
| `lua_isstring`          | `lua_isstring`          |   ✓   |   ✓     |   ✓    |
| `lua_istable`           | `lua_istable`           |   M   |   ✓     |   ✓    |
| `lua_isthread`          | `lua_isthread`          |   M   |   ✓     |   ✓    |
| `lua_isuserdata`        | `lua_isuserdata`        |   ✓   |   ✓     |        |
| `lua_isyieldable`       | `lua_isyieldable`       |   ✓   |   ✓     |   ✓    |
| `lua_len`               | `lua_len`               |   ✓   |         |   ✓    |
| `lua_load`              | `lua_load`              |   ✓   |         |        |
| `lua_newstate`          |                         |       |         |        |
| `lua_newtable`          | `lua_newtable`          |   M   |   ✓     |   ✓    |
| `lua_newthread`         | `lua_newthread`         |   ✓   |         |        |
| `lua_newuserdata`       | `lua_newuserdata`       |   ✓   |         |        |
| `lua_next`              | `lua_next`              |   ✓   |         |        |
| `lua_numbertointeger`   | `lua_numbertointeger`   |   M   |   ✓     |        |
| `lua_pcall`             | `lua_pcall`             |   M   |   ✓     |   ✓    |
| `lua_pcallk`            | `lua_pcallk`            |   ✓   |         |        |
| `lua_pop`               | `lua_pop`               |   M   |   ✓     |   ✓    |
| `lua_pushboolean`       | `lua_pushboolean`       |   ✓   |         |        |
| `lua_pushcclosure`      | `lua_pushcclosure`      |   ✓   |         |        |
| `lua_pushcfunction`     | `lua_pushcfunction`     |   M   |   ✓     |        |
| `lua_pushfstring`       |                         |   w   |         |        |
| `lua_pushglobaltable`   |                         |       |         |        |
| `lua_pushinteger`       | `lua_pushinteger`       |   ✓   |         |   ✓    |
| `lua_pushlightuserdata` | `lua_pushlightuserdata` |   ✓   |         |        |
| `lua_pushliteral`       | `lua_pushliteral`       |   M   |   ✓     |        |
| `lua_pushlstring`       | `lua_pushlstring`       |   ✓   |         |        |
| `lua_pushnil`           | `lua_pushnil`           |   ✓   |         |   ✓    |
| `lua_pushnumber`        | `lua_pushnumber`        |   ✓   |         |   ✓    |
| `lua_pushstring`        | `lua_pushstring`        |   ✓   |         |   ✓    |
| `lua_pushthread`        | `lua_pushthread`        |   ✓   |         |        |
| `lua_pushvalue`         | `lua_pushvalue`         |   ✓   |         |   ✓    |
| `lua_pushvfstring`      |                         |   w   |         |        |
| `lua_rawequal`          | `lua_rawequal`          |   ✓   |         |        |
| `lua_rawget`            | `lua_rawget`            |   ✓   |         |        |
| `lua_rawgeti`           | `lua_rawgeti`           |   ✓   |         |        |
| `lua_rawgetp`           | `lua_rawgetp`           |   ✓   |         |        |
| `lua_rawlen`            | `lua_rawlen`            |   ✓   |         |        |
| `lua_rawset`            | `lua_rawset`            |   ✓   |         |        |
| `lua_rawseti`           | `lua_rawseti`           |   ✓   |         |        |
| `lua_rawsetp`           | `lua_rawsetp`           |   ✓   |         |        |
| `lua_remove`            | `lua_remove`            |   M   |   ✓     |        |
| `lua_replace`           | `lua_replace`           |   M   |   ✓     |        |
| `lua_rotate`            | `lua_rotate`            |   ✓   |   ✓     |   ✓    |
| `lua_register`          | `lua_register`          |   M   |   ✓     |        |
| `lua_resume`            | `lua_resume`            |   ✓   |   ✓     |   ✓    |
| `lua_setallocf`         |                         |       |         |        |
| `lua_setglobal`         | `lua_setglobal`         |   ✓   |         |        |
| `lua_setfield`          | `lua_setfield`          |   M   |   ✓     |   ✓    |
| `lua_seti`              | `lua_seti`              |   ✓   |         |        |
| `lua_setmetatable`      | `lua_setmetatable`      |   ✓   |         |        |
| `lua_settable`          | `lua_settable`          |   ✓   |         |        |
| `lua_settop`            | `lua_settop`            |   ✓   |         |        |
| `lua_setuservalue`      | `lua_setuservalue`      |   ✓   |         |        |
| `lua_status`            | `lua_status`            |   ✓   |         |   ✓    |
| `lua_stringtonumber`    | `lua_stringtonumber`    |   ✓   |   ✓     |        |
| `lua_toboolean`         | `lua_toboolean`         |   ✓   |   ✓     |   ✓    |
| `lua_tocfunction`       | `lua_tocfunction`       |   ✓   |         |        |
| `lua_tointeger`         | `lua_tointeger`         |   M   |   ✓     |   ✓    |
| `lua_tointegerx`        | `lua_tointegerx`        |   ✓   |         |        |
| `lua_tonumber`          | `lua_tonumber`          |   M   |   ✓     |   ✓    |
| `lua_tonumberx`         | `lua_tonumberx`         |   ✓   |         |        |
| `lua_topointer`         | `lua_topointer`         |   ✓   |         |        |
| `lua_tothread`          | `lua_tothread`          |   ✓   |         |   ✓    |
| `lua_touserdata`        | `lua_touserdata`        |   ✓   |         |        |
| `lua_tolstring`         | `lua_tolstring`         |   ✓   |         |        |
| `lua_tostring`          | `lua_tostring`          |       |   ✓     |   ✓    |
| `lua_type`              | `lua_type`              |   ✓   |         |        |
| `lua_typename`          | `lua_typename`          |   ✓   |   ✓     |   ✓    |
| `lua_upvalueindex`      | `lua_upvalueindex`      |   ✓   |         |        |
| `lua_version`           | `lua_version`           |   ✓   |   ✓     |   ✓    |
| `lua_xmove`             | `lua_xmove`             |   ✓   |   ✓     |        |
| `lua_yield`             | `lua_yield`             |   M   |   ✓     |        |
| `lua_yieldk`            | `lua_yieldk`            |   ✓   |         |        |

### Auxiliary Functions

| C Aux Function Name     | Fortran Interface Name  | Bound | Wrapped | Tested |
|-------------------------|-------------------------|-------|---------|--------|
| `luaL_addchar`          |                         |   M   |         |        |
| `luaL_addlstring`       |                         |       |         |        |
| `luaL_addsize`          |                         |   M   |         |        |
| `luaL_addstring`        |                         |       |         |        |
| `luaL_addvalue`         |                         |       |         |        |
| `luaL_argcheck`         |                         |   M   |         |        |
| `luaL_argerror`         |                         |       |         |        |
| `luaL_buffinit`         |                         |       |         |        |
| `luaL_buffinitsize`     |                         |       |         |        |
| `luaL_callmeta`         | `lual_callmeta`         |   ✓   |         |        |
| `luaL_checkany`         | `lual_checkany`         |   ✓   |         |        |
| `luaL_checkinteger`     | `lual_checkinteger`     |   ✓   |         |        |
| `luaL_checklstring`     | `lual_checklstring`     |   ✓   |    ✓    |        |
| `luaL_checknumber`      | `lual_checknumber`      |   ✓   |    ✓    |        |
| `luaL_checkoption`      |                         |       |         |        |
| `luaL_checkstack`       | `lual_checkstack`       |   ✓   |         |        |
| `luaL_checkstring`      | `lual_checkstring`      |   M   |    ✓    |        |
| `luaL_checktype`        | `lual_checktype`        |   ✓   |         |        |
| `luaL_checkudata`       | `lual_checkudata`       |   ✓   |         |        |
| `luaL_checkversion`     |                         |   M   |         |        |
| `luaL_dofile`           | `lual_dofile`           |   M   |    ✓    |   ✓    |
| `luaL_dostring`         | `lual_dostring`         |   M   |    ✓    |   ✓    |
| `luaL_error`            | `lual_error`            |   ✓   |         |   ✓    |
| `luaL_execresult`       | `lual_execresult`       |   ✓   |         |        |
| `luaL_fileresult`       | `lual_fileresult`       |   ✓   |         |        |
| `luaL_getmetafield`     | `luaL_getmetafield`     |   ✓   |         |        |
| `luaL_getmetatable`     |                         |   M   |         |        |
| `luaL_getsubtable`      | `lual_getsubtable`      |   ✓   |         |        |
| `luaL_gsub`             |                         |   w   |         |        |
| `luaL_loadbuffer`       |                         |   M   |         |        |
| `luaL_loadbufferx`      |                         |       |         |        |
| `luaL_loadfile`         | `lual_loadfile`         |   M   |    ✓    |        |
| `luaL_loadfilex`        | `lual_loadfilex`        |   ✓   |         |        |
| `luaL_loadstring`       | `lual_loadstring`       |   ✓   |    ✓    |   ✓    |
| `luaL_newlib`           |                         |   M   |         |        |
| `luaL_newlibtable`      |                         |   M   |         |        |
| `luaL_newmetatable`     | `lual_newmetatable`     |   ✓   |         |        |
| `luaL_newstate`         | `lual_newstate`         |   ✓   |         |   ✓    |
| `luaL_openlibs`         | `lual_openlibs`         |   ✓   |         |   ✓    |
| `luaL_opt`              |                         |   M   |         |        |
| `luaL_optinteger`       |                         |       |         |        |
| `luaL_optlstring`       |                         |       |         |        |
| `luaL_optnumber`        |                         |       |         |        |
| `luaL_optstring`        |                         |   M   |         |        |
| `luaL_prepbuffer`       |                         |   M   |         |        |
| `luaL_prepbuffsize`     |                         |       |         |        |
| `luaL_pushresult`       |                         |       |         |        |
| `luaL_pushresultsize`   |                         |       |         |        |
| `luaL_ref`              | `lual_ref`              |   ✓   |         |        |
| `luaL_register`         |                         |   M   |         |        |
| `luaL_requiref`         |                         |       |         |        |
| `luaL_setfuncs`         |                         |       |         |        |
| `luaL_setmetatable`     | `luaL_setmetatable`     |   ✓   |         |        |
| `luaL_testudata`        | `luaL_testudata`        |   ✓   |         |        |
| `luaL_tolstring`        | `luaL_tolstring`        |   M   |    ✓    |   ✓    |
| `luaL_traceback`        | `luaL_traceback`        |   ✓   |         |        |
| `luaL_typename`         | `lual_typename`         |   M   |    ✓    |   ✓    |
| `luaL_unref`            | `lual_unref`            |   ✓   |         |        |
| `luaL_where`            | `luaL_where`            |   ✓   |         |        |

### Unimplemented C API Functions

* `lua_atpanic` - panic functions not supported; requires C implementation
* `lua_dump` - requires lua_Writer which requires C implementation
* `lua_getallocf` - `lua_Alloc` not supported;  requires C implementation
* `lua_getextraspace` - requires `LUA_EXTRASPACE` from `lua.h`; not exported
* `lua_newstate` - `lua_Alloc` not supported; use `luaL_newstate`
* `lua_pushfstring` - Fortran does not support variadic functions like fstring. Workaround: compose string with format() then use `lua_pushstring`
* `lua_pushglobaltable` - requires `LUA_REGISTRYINDEX` and `LUA_RIDX_GLOBALS` (= 2) from `lua.h`; not exported.
* `lua_pushvfstring` - Fortran does not support variadic functions like fstring. Workaround: compose string with format() then use `lua_pushstring`
* `lua_setallocf` - `lua_Alloc` not supported

### Unimplemented Auxiliary Functions

* `luaL_addchar` - buffer operations not currently supported
* `luaL_addlstring` - buffer operations not currently supported
* `luaL_addsize` - buffer operations not currently supported
* `luaL_addstring` - buffer operations not currently supported
* `luaL_addvalue` - buffer operations not currently supported
* `luaL_argcheck` - C macro; error linking to C API function `luaL_argerror` - extern?
* `luaL_argerror` - error linking to C API function `luaL_argerror` - extern?
* `luaL_buffinit` - buffer operations not currently supported
* `luaL_buffinitsize` - buffer operations not currently supported
* `luaL_checkoption` - deals with arguments passed by Lua to an imported Fortran function
* `luaL_checkversion` - C macro; underlying function and constants `LUA_VERSION_NUM` and `LUAL_NUMSIZES` may not be accessible
* `luaL_getmetatable` - C macro; requires `LUA_REGISTRYINDEX` from `lua.h`; not exported
* `luaL_gsub` - Workaround: Perform text manipulation in Fortran
* `luaL_loadbuffer` - C macro; buffer operations not currently supported
* `luaL_loadbufferx` - buffer operations not currently supported
* `luaL_newlib` - C macro; relies on `luaL_checkversion`, `luaL_newlibtable`, and unimplemented data structure `luaL_Reg`
* `luaL_newlibtable` - C macro; relies on unimplemented data structure `luaL_Reg`
* `luaL_opt` - C macro; deals with arguments passed by Lua to an imported Fortran function
* `luaL_optinteger` - deals with arguments passed by Lua to an imported Fortran function
* `luaL_optlstring` - deals with arguments passed by Lua to an imported Fortran function
* `luaL_optnumber` - deals with arguments passed by Lua to an imported Fortran function
* `luaL_optstring` - C macro; deals with arguments passed by Lua to an imported Fortran function
* `luaL_prepbuffer` - buffer operations not currently supported
* `luaL_prepbuffsize` - buffer operations not currently supported
* `luaL_pushresult` - buffer operations not currently supported
* `luaL_pushresultsize` - buffer operations not currently supported
* `luaL_requiref` - access to `require` and package directory is not supported in Fortran
* `luaL_setfuncs` - relies on unimplemented data structure `luaL_Reg`

### Unimplemented Debug API functions

The Debug API is not supported:

* `lua_gethook`
* `lua_gethookcount`
* `lua_gethookmask`
* `lua_getinfo`
* `lua_getlocal`
* `lua_getstack`
* `lua_getupvalue`
* `lua_sethook`
* `lua_setlocal`
* `lua_setupvalue`
* `lua_upvalueid`
* `lua_upvaluejoin`

## Licence

ISC; see LICENCE.md, based on the SPDX license template at https://spdx.org/licenses/ISC.html
